FROM python:3.10-slim as packager
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP="weather_website"
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /app/
# Enable venv
RUN python -m venv /opt/venv
# Install build tools, because uWSGI may need to be built (for e.g. ARM)
RUN apt-get update && apt-get install -y build-essential python3-dev
RUN pip install pyuwsgi

COPY ./setup.* ./pyproject.toml ./README.md /app/

COPY ./weather_website/ /app/weather_website/

RUN pip install build
RUN python -m build

FROM python:3.10-slim as runner
WORKDIR /app/
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP="weather_website"
ENV PATH="/opt/venv/bin:$PATH"

# Setup user for serving webcontent
RUN groupadd -g 777 webdata && groupadd -g 169 webserver && groupadd -g 170 backend && useradd -u 169 -g 169 -G webdata webserver && useradd -u 170 -g 170 -G webdata backend
RUN apt-get update && apt-get install -y netcat-traditional
COPY ./scripts/docker-link-dirs.sh /app/
COPY ./scripts/wait-for /

# Copy python venv with installed pyuwsgi
COPY --from=packager /opt/venv/ /opt/venv/

# Can only link dirs properly after new venv is installed ofc
RUN sh /app/docker-link-dirs.sh
COPY --from=packager /app/dist/ /app/dist/
RUN pip install --find-links=dist/ "weather_website[uWSGI]"

# COPY instance/ /app/instance/
# If server can't access the instance dir (and thus fails to start), use docker-chown-fix scripts.
USER webserver
CMD ["waitress-serve", "--call", "weather_website:launch_app"]
