#!/bin/sh
# Setup Docker configs
# Run again if you change a config here; the containers need to be not running to be able to update the configs in docker.

# Requires Docker in swarm mode
echo This script requires the following configs already created from their example confs:
echo    - nginx/nginx.conf
echo    - nginx/ssl-params.conf
echo    - mqtt/mosquitto.conf

echo " "
echo "The config.py for weather-website will be copied from the package root (if not already set up) on startup."
echo "It won't be setup here, but should be adapted to your wishes before deploying the stack."

echo Ready to continue? Press ENTER

read ok

docker swarm init
echo Setting up nginx main config...
docker config create nginxconf ./nginx/nginx.conf 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm nginxconf
    docker config create nginxconf ./nginx/nginx.conf
fi

echo Setting up nginx ssl config extras...
docker config create nginxconf2 ./nginx/ssl-params.conf 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm nginxconf2
    docker config create nginxconf2 ./nginx/ssl-params.conf
fi

echo Setting up mosquitto config...
docker config create mqconfig ./mqtt/config/mosquitto.conf 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm mqconfig
    docker config create mqconfig ./mqtt/config/mosquitto.conf
fi

echo Setting up mosquitto logrotate config...
docker config create mqlogrotate ./mqtt/config/mosquitto 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm mqlogrotate
    docker config create mqlogrotate ./mqtt/config/mosquitto
fi

echo Setting up mosquitto startup script...
docker config create mqstartup ./scripts/mosquitto-startup.sh 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm mqstartup
    docker config create mqstartup ./scripts/mosquitto-startup.sh
fi

echo Setting up uwsgiconf...
docker config create uwsgiconf ./uwsgi.ini 2>/dev/null
if [ $? -ne 0 ]
then
    docker config rm uwsgiconf
    docker config create uwsgiconf ./uwsgi.ini
fi

echo Done!
