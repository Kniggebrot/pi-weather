#!/usr/bin/env bash
# Run backend from virtual python environment.

source venv/bin/activate && export FLASK_APP=weather_website && waitress-serve --call 'weather_website:launch_app'
