#!/usr/bin/env python3

"""
Read measurements from a file and paste them into a sqlite database.
Should be put in a separate database, then pasted merged into a main database, if wished
"""

import json
import weather_website.weather_store as weather_store

db = weather_store.get_db("sep.db")

with open("text.txt", "r") as text:
    for line in text:
        decoded = json.loads(line)
        decoded["timestamp"] = int(decoded["timestamp"], 16)
        if decoded.get("chiptemp"):
            decoded["voltage"] = decoded.pop("vcc")

            if decoded.get("name") is None:
                decoded["name"] = None
            if decoded.get("lastreset") is None:
                decoded["lastreset"] = None
            db.add_status(decoded)
        else:
            db.add_measurement(decoded)
