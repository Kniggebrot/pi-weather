# Python weather

Receiving the measurements sent by [esp-sensor](https://gitlab.com/Kniggebrot/esp-sensor) via MQTT and putting them in a SQLite database.
Also hosting a very basic website on which the last measurement and status of a device can be seen.

## Setup

This application requires a setup config.py for the flask app. If you want to use custom settings for it (see [here](https://flask.palletsprojects.com/en/2.1.x/config/)), you should insert them in the provided config.py before the first start.
When no config.py is found in the apps instance folder, the repo's [config.py](weather_website/config.py) will then be copied into its instance folder and appended with a secret key (if not already provided), saving you the "hassle" of generating one.

## Usage

There are two ways to use this app: Via Docker or manually via a virtualenv.

### Docker

Ensure Docker is running on your machine. With standard configuration, the server requires certificates to verify allowed clients outside of the app: Place your CA certificate as well as your server certificate and key under `mqtt/certs`. If you prefer a different authentication method, you can edit [mosquitto.conf](mqtt/config/mosquitto.conf) under `mqtt/config`. Info on its options can be found [here](https://mosquitto.org/man/mosquitto-conf-5.html).

Finally, run `docker-compose -f "docker-compose.yml" up` in the project directory. Docker will build the necessary images and run the web and MQTT server.

If you want to have the SQLite DBs in a local folder instead of a volume, uncomment line __14__ in [docker-compose.yml](docker-compose.yml#l14) and comment out line __15__ instead. It should look like this:

```yaml
[...]

    volumes:
      #- db:/app/instance
      - ./instance:/app/instance # allows to see and edit the db on your filesystem on the go

[...]
```

With standard config, the MQTT server can be reached on port 8883 by clients with proper certificates.

If you want to debug the application in a docker container, you can use `docker compose -f "docker-compose.debug.yml" up` instead, and attach a debugpy instance (for example, via [VS Code](https://blog.theodo.com/2020/05/debug-flask-vscode/)) to port 5678. The "weather-website", "mqtt" and "instance" directories are bound to the containers, so any changes in the repo will be directly visible in the docker application. Many thanks to Adrien Cacciaguerra and his [blog post](https://blog.theodo.com/2020/05/debug-flask-vscode/).

### Virtualenv

A python virtualenv can also be used to run `pi-weather`. However, it will not run a MQTT server alongside it; you have to provide one.

Before first use, run [init_virtualenv.bat](init_virtualenv.bat) or [init_virtualenv.sh](init_virtualenv.sh) (depending if you're on Windows or Linux) to initialize the virtual environment and install the required packages.
Then, edit the host for the MQTT listener under [weather_website/weather_receive.py](weather_website/weather_receive.py#l81) in line **81** and **85**:

```python
[...]

        
        try:
        #    Use this if running a MQTT server on your local machine:
        #   listener1.connect(host="localhost")
        _connerror_handler(listener=listener1, host="<URI of your MQTT server>", port=<Port of your MQTT server>)
        except gaierror:
        # Assuming script is running outside of a docker container
        try:
                _connerror_handler(listener=listener1, host="<URI of your MQTT server>", port=<Port of your MQTT server>)

[...]
```

After this you can run the application via `venv_run_backend.bat` or `venv_run_backend.sh`.
