Put your webserver certificate and key (nginx.key & nginx.cert) here. On build, docker will store them encrypted as a secret and only provide it as a ramdisk to nginx.
Also, it is recommended to generate a Diffie-Hellman group (dhparam.pem) and put it here as well.
