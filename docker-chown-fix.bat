:: Fix user and group owners in pi-weather_db volume.
:: Only works with a created pi-weather_db, ofc
@echo off
set scriptdir=%cd%\scripts\
docker run --rm --user root -v pi-weather_db:/data -v "%scriptdir%:/scripts/:ro" alpine sh /scripts/docker-chown-website.sh
