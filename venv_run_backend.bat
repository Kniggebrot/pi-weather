:: Run backend from virtual python environment.

@echo off
call venv\Scripts\activate.bat && set FLASK_APP=weather_website && waitress-serve --call 'weather_website:launch_app'
