#!/usr/bin/env bash
# Init a virtualenv for the backend, and install its production requirements

python -m venv venv && source venv/bin/activate
pip install build
python -m build
pip install --no-index --find-links=dist/ weather_website
export FLASK_APP=weather_website
echo Initialized virtualenv.
