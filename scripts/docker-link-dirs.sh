#!/bin/sh
# With the installation via wheel in production, the instance path has changed as well.
# This script should be run on container init to hard link the new dir to the old one.

python_dir=$( python -c "import sys; print(sys.prefix)" )
if [ ${python_dir:-1} = "/" ]
then
    instance_dir="$python_dir"
else
    instance_dir="$python_dir"/
fi

#echo $instance_dir
mkdir -p "$instance_dir"var
instance_dir="$instance_dir"var/weather_website-instance
ln -s "/app/instance/" "$instance_dir"
