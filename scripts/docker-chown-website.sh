#!/bin/sh
# Change permissions for files which will be changed by server, set group to webdata for files (dbs) edited by backend
# DO NOT EXECUTE OUTSIDE OF DOCKER ALPINE CONTAINER!
echo Creating user server, group webdata...
addgroup -g 777 webdata
addgroup -g 169 webserver
addgroup -g 170 backend
adduser -D -H -G webserver -u 169 webserver # No password, no home dir
adduser -D -H -G backend -u 170 backend # No password, no home dir
echo Touching files...
touch /data/user.db
touch /data/test.db
touch /data/backup.db
touch /data/config.py
echo "Setting permissions for instance dir in general..."
chown -R :webdata /data/
chmod 775 /data/
echo "Setting extra permissions for website-only dirs..."
chown -R webserver: /data/config.py /data/logs/app_log.log*
chown -R backend: /data/logs/backend.log*
chmod 644 /data/config.py /data/logs/?*
chmod 775 /data/logs/
chown webserver:webdata /data/user.db
echo "Set users to backend for weather and backup db..."
chown backend:webdata /data/backup.db /data/test.db
