:: Init a virtualenv for the backend, and install its production requirements

@echo off
py -3 -m venv venv && call venv\Scripts\activate.bat
pip install build
python -m build
pip install --no-index --find-links=dist/ weather_website
set FLASK_APP=weather_website
echo Initialized virtualenv.
