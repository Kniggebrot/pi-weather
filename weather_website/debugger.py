# Debugger for python container
from os import getenv

def init_debugger_if_needed():
    if getenv("DEBUGGER") == "YES":
        import multiprocessing

        if multiprocessing.current_process().pid > 1:
            import debugpy
            import logging

            logging.getLogger(__package__).setLevel(logging.DEBUG)
            logging.getLogger('werkzeug').setLevel(logging.DEBUG)

            debugpy.listen(("0.0.0.0", 5678))
            print("VS Code debugger can now be attached, press F5 in VS Code")
            debugpy.wait_for_client()
            print("VS Code debugger attached, enjoy debugging")
