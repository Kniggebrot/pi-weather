# Functions for launching processes for MQTT receiving/sending
import argparse
import logging, logging.handlers
import os
import multiprocessing as mp
from pathlib import Path
import signal
import sys
from threading import Thread
from time import sleep
import weather_website.backend.weather_receive as we_r
import weather_website.backend.send_downlink as we_s
from weather_website.setup_logging import setup_file_logging_backend

from weather_website import listener, logqueue

logger = logging.getLogger("weather_website.backend")
logger.setLevel(logging.INFO)
if not logger.hasHandlers():
    logger.addHandler( logging.handlers.QueueHandler(logqueue) )

def receiver_thread(*args):
    receiver = Thread(target=we_r.mainmqtt, args=args, daemon=True)
    receiver.do_run = True
    receiver.start()

    def end_receiver(signum, stack):
        receiver.do_run = False
        receiver.join(3)
        sys.exit()

    signal.signal(signal.SIGINT, end_receiver)
    signal.signal(signal.SIGTERM, end_receiver)
    errorcnt = 0
    while errorcnt < 10:
        sleep(10)
        if not receiver.is_alive() and receiver.do_run:
            logger.error("MQTT receive thread stopped unexpectedly!")
            logger.error("Restarting receive thread in 10s...")
            sleep(1)
            errorcnt += 1
            receiver = None # "delete" old receiver thread
            receiver = Thread(target=we_r.mainmqtt, args=args, daemon=True)
            receiver.do_run = True
            receiver.start()
    if errorcnt >= 10:
        logger.critical("MQTT receive thread failed after 10 tries!")
    receiver.do_run = False
    receiver.join(3)

def downlinker_thread(*args):
    downlinker = Thread(target=we_s.config_via_mqtt, args=args, daemon=True)
    downlinker.do_run = True
    downlinker.start()

    def end_downlinker(signum, stack):
        downlinker.do_run = False
        downlinker.join(10)
        sys.exit()

    signal.signal(signal.SIGINT, end_downlinker)
    signal.signal(signal.SIGTERM, end_downlinker)
    errorcnt = 0
    while errorcnt < 10:
        sleep(10)
        if not downlinker.is_alive() and downlinker.do_run:
            logger.error("Downlink thread stopped unexpectedly!")
            logger.error("Restarting downlink thread in 10s...")
            sleep(1)
            errorcnt += 1
            downlinker = None # "delete" old receiver thread
            downlinker = Thread(target=we_s.config_via_mqtt, args=args, daemon=True)
            downlinker.do_run = True
            downlinker.start()
    if errorcnt >= 10:
        logger.critical("Downlink thread failed after 10 tries!")
    downlinker.do_run = False
    downlinker.join(3)

# Convenience function for launching MQTT receive process.
def launch_receiver(logqueue: mp.JoinableQueue, measure_db: (str|Path)=None, backup_db: (str|Path)=None, hostname: str=None):
    # If args are unset, get them from env
    if not measure_db:
        measure_db = os.getenv("FLASK_DB_WEATHER")
        if not measure_db:
            raise Exception("No measure DB to write to set!")
    if not backup_db:
        backup_db = os.getenv("FLASK_BACKUP_WDB")
        # No backup set? No backup made
    if not hostname:
        hostname = os.getenv("FLASK_MQTTHOST")
        if not hostname:
            raise Exception("No MQTT host set!")

    logger.info(f"Receiver: Host {hostname}, DB {measure_db}, Backup {backup_db}")
    receiver = mp.Process(target=receiver_thread, args=(measure_db, backup_db, hostname, False, logqueue), daemon=True)
    receiver.start()

    return receiver

def launch_downlinker(logqueue: mp.Queue, listenerPort: int, listenerName: str, hostname: str=None, user_db: (str|Path)=None, backup_db: (str|Path)=None):
    # If args are unset, get them from env
    if not user_db:
        user_db = os.getenv("FLASK_DB_USER")
        if not user_db:
            raise Exception("No user DB to read downlinks from set!")
    if not backup_db:
        backup_db = os.getenv("FLASK_BACKUP_UDB")
        # No backup set? No backup made
    if not hostname:
        hostname = os.getenv("FLASK_MQTTHOST")
        if not hostname:
            raise Exception("No MQTT host set!")
    if not listenerName:
        listenerName = os.getenv("FLASK_LISTENERNAME")
        if not listenerName:
            listenerName = "localhost"
    if not listenerPort:
        listenerPort = os.getenv("FLASK_LISTENERPORT")
        if not listenerPort:
            raise Exception("No port for downlink listener set!")
    listenerPort = int(listenerPort)

    logger.info(f"Downlinker: Sending to mqtt host {hostname}, listening on {listenerName}:{listenerPort}, UDB {user_db}, Backup {backup_db}")
    downlinker = mp.Process(target=downlinker_thread, args=(hostname, listenerName, listenerPort, user_db, backup_db, logqueue), daemon=True)
    downlinker.start()

    return downlinker

def launch_from_cli():
    parser = argparse.ArgumentParser(description="Launch backend processes for weather_website.")
    parser.add_argument("launching", choices=("all", "downlink", "receiver"), default="all", nargs='?', help="Launch MQTT downlink and/or measurement receiver processes")

    parser.add_argument("-H", "--hostname", type=str, help="MQTT host to connect to")
    parser.add_argument("-w", "--weather-db", type=Path, help="Path to weather measurements DB")
    parser.add_argument("-bw", "--backup-wdb", type=Path, help="Path to backup weather measurements DB")
    parser.add_argument("-u", "--user-db", type=Path, help="Path to user/downlinks DB")
    parser.add_argument("-bu", "--backup-udb", type=Path, help="Path to backup user/downlinks DB")
    parser.add_argument("-lH", "--listen-hostname", type=str, help="Hostname/IP on which downlink process should listen for new downlinks")
    parser.add_argument("-lP", "--listen-port", type=int, help="Port on which downlink process should listen for new downlinks")
    
    args = parser.parse_args()

    receiver: mp.Process=None
    downlinker: mp.Process=None
    logdir: Path=None
    
    if args.weather_db:
        logdir = args.weather_db.parent / "logs"
    elif args.user_db:
        logdir = args.user_db.parent / "logs"
    elif os.getenv("FLASK_DB_WEATHER"):
        logdir = Path(os.getenv("FLASK_DB_WEATHER")).parent / "logs"
    elif os.getenv("FLASK_DB_USER"):
        logdir = Path(os.getenv("FLASK_DB_USER")).parent / "logs"

    if logdir:
        logger.info(f"Logging to {logdir}")
        filehandler = setup_file_logging_backend(logdir)
        listener.addHandler(filehandler)

    if not args.launching == "all":
        if args.launching == "receiver":
            receiver = launch_receiver(logqueue, args.weather_db, args.backup_wdb, args.hostname)
        elif args.launching == "downlink":
            downlinker = launch_downlinker(logqueue, args.listen_port, args.listen_hostname, args.hostname, args.user_db, args.backup_udb)
    else:
        receiver = launch_receiver(logqueue, args.weather_db, args.backup_wdb, args.hostname)
        downlinker = launch_downlinker(logqueue, args.listen_port, args.listen_hostname, args.hostname, args.user_db, args.backup_udb)
    
    def stop_backend(signalnum, stack):
        logger.warning(f"Terminating backend due to received signal {signalnum}.")
        if downlinker:
            logger.warning("Stopping downlinker...")
            downlinker.join(3)
            if downlinker.is_alive():
                downlinker.terminate()
        if receiver:
            logger.warning("Stopping receiver...")
            receiver.join(3)
            if receiver.is_alive():
                receiver.terminate()
        logger.info("Backend done.")
        sys.exit()

    signal.signal(signal.SIGINT, stop_backend)
    signal.signal(signal.SIGTERM, stop_backend)
    if os.name == "posix":
        signal.pause()
    else:
        while 1:
            sleep(10000)

if __name__ == "__main__": # pragma: no coverage
    launch_from_cli()