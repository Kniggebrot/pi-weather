"""
Send config downlinks to esp-sensors.

Since this module might be executed on a completely different machine/docker container, the thread is notified via a tcp socket of a new downlink in the DB.
"""

import base64
import json
import logging, logging.handlers
import paho.mqtt.client as mqtt
from pathlib import Path
import socketserver
import threading
from time import sleep

from weather_website.user_db import UserDB
from weather_website.backend.weather_receive import _connerror_handler

logger = logging.getLogger(__name__)
#logger.setLevel(logging.INFO)

dlList: dict = {}

class DownlinkHandler(socketserver.BaseRequestHandler):
    """
    Handle downlink requests from frontend.

    Message Format: 4 chars for message length, then message with message length
    """
    def handle(self):

        def send_withlen(msg: str, socket):
            """
            Send base64-encoded message via socket with prefaced msglen (4 digits)
            """
            b64msg = base64.b64encode(bytes(msg, "utf-8"))
            msglen = str(len(b64msg))
            msglen = msglen.ljust(4)

            fullmsg = f"{msglen}{b64msg}"
            fullmsg = bytes(fullmsg, "ascii")
            socket.sendall(fullmsg)

        def send_config(client: mqtt.Client, userdata, message: mqtt.MQTTMessage):
            "MQTT callback for sending new config after first status message of device"
            device = message.topic.split("/")[2]
            
            downlink = dlList[device]
            if downlink:
                client.publish(f"/sensor/{device}/config", downlink["downlink"], qos=1)
            else:
                logger.error(f"No downlink for device {device} found")
            client.message_callback_remove(message.topic)
        
        assert self.server.udb
        assert self.server.mqtt

        socket = self.request
        clientadr = self.client_address

        client: mqtt.Client = self.server.mqtt
        userdb: UserDB = self.server.udb

        self.datalen_raw = socket.recv(4).strip()
        try:
            self.datalen = int(self.datalen_raw)
        except Exception as e:
            logger.error(f"Error finding datalen: {e}")
            logger.error(f"Client: {clientadr}")
            send_withlen(b"ERROR", socket)
            socket.sendall(b"")
        else:
            self.dataraw = socket.recv(self.datalen).strip()
            try:
                self.data = base64.b64decode(self.dataraw, validate=True)
            except:
                logger.debug("Failed to decode data from Base64")
                self.data = self.dataraw
            logger.info(f"Received {self.datalen} bytes from {clientadr}: {self.data}")
            if "SEND" in self.data:
                try:
                    sendstr = self.data.decode("utf-8")
                    sendid = int(sendstr.split()[1])
                    downlink = userdb.get_downlink(sendid)
                    if downlink:
                        logger.debug(f"New downlink for {downlink['device_id']}: {downlink['downlink']}")
                        # Extract device from downlink dict, add new config to dlList
                        device = downlink["device_id"]
                        if not dlList[device]:
                            dlList[device] = {"id": sendid, "downlink": downlink["downlink"]}
                        else:
                            # Remove old config, set mqtt_sent value to indicate skipped downlink
                            oldid = dlList[device]["id"]
                            userdb.update_downlink_mqttsent(oldid, userdb.MQTT_DOWNLINK_SKIPPED)
                        # Add callback when device sends next status message, send config
                        client.message_callback_add(f"/sensor/{device}/status", send_config)
                except Exception as e:
                    logger.error(f"Failed to compute downlink message: {e}")
                    logger.error(f"Client: {clientadr}")
                    send_withlen(b"ERROR", socket)
                    socket.sendall(b"")
                else:
                    send_withlen(f"OK {sendid}", socket)

# The callback for when the client receives a CONNACK response from the server.
def _on_connect(client, userdata, flags, rc):
    logger.info("Connected with result code "+str(rc))
    client.subscribe("/sensor/+/status")

def downlink_task(client: mqtt.Client, user_dbpath: Path, listener_host: str, port: int):
    with socketserver.TCPServer( (listener_host, port), DownlinkHandler) as listener:
        listener.udb = UserDB(user_dbpath)
        listener.mqtt = client
        self_thread = threading.current_thread()
        server_thread = threading.Thread(target=listener.serve_forever, name="socketserver", daemon=True)
        server_thread.start()
        while getattr(self_thread, "do_run", False):
            pass
        listener.shutdown()
        server_thread.join()

def config_via_mqtt(mqtt_hostname: str, listen_on_host: str, listen_on_port: int, user_db: str|Path, backup: str|Path=None, logqueue=None):
    # Launch as a process
    if (not logger.hasHandlers()) and logqueue:
        queuehdlr = logging.handlers.QueueHandler(logqueue)
        logger.addHandler( queuehdlr )
    dltask = None
    udb = UserDB(user_db, backup, logqueue=logqueue)
    sender = mqtt.Client()
    sender.on_connect = _on_connect
    self_process = threading.current_thread()
    while getattr(self_process, 'do_run', False):
        if not sender.is_connected():
            try:
                _connerror_handler(listener=sender, host=mqtt_hostname)
            except: # pragma: no coverage
                logger.critical(f"Couldn't connect to host {mqtt_hostname}.")
                logger.critical("As no connection was possible after 5 tries, the process is killed.")
                break
            else:
                logger.info("Downlinks are being sent to host %s, listening on %s:%d", mqtt_hostname, listen_on_host, listen_on_port)
                if not dltask:
                    dltask = threading.Thread(target=downlink_task, args=(sender, udb.dbpath, listen_on_host, listen_on_port), daemon=True)
                    dltask.do_run = True
                    dltask.start()
        else:
            sleep(1)
    logger.warning("Stopping Downlink Thread.")
    dltask.do_run = False
    dltask.join(10)
    sender.disconnect()
    sender.loop_stop()
    if hasattr(udb, "backup_thread"):
        udb.backup_thread.do_run = False
    udb = None
    del udb
    logger.warning("Downlink thread stopped.")
