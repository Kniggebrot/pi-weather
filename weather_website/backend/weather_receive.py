#!/usr/bin/env python3
"""
Receive measurements via MQTT, parse them and put them in the DB
"""

from multiprocessing import Queue
from pathlib import Path
import threading
from typing import Optional
import paho.mqtt.client as mqtt
from time import sleep, time
import json
import logging, logging.handlers

import weather_website.weather_store as we_s

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# The callback for when the client receives a CONNACK response from the server.
def _on_connect(client, userdata, flags, rc):
    logger.info("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/sensor/#")

def _on_config(client, userdata: we_s.MeasureDB, msg):
    #logger.verbose(msg.topic+" "+str(msg.payload))
    logger.debug("Receveived PUBLISH!")
    logger.debug("From: " + msg.topic)

    decoded= json.loads(msg.payload)
    devId = decoded.pop("id")
    cfgEntry = {
        "id": devId,
        "seen_at": int(time()),
        "cfg_json": json.dumps(decoded),
    }
    userdata.add_config(cfgEntry)

# The callback for when a PUBLISH message is received from the server.
def _on_measure(client, userdata: we_s.MeasureDB, msg):
    #logger.verbose(msg.topic+" "+str(msg.payload))
    logger.debug("Receveived PUBLISH!")
    logger.debug("From: " + msg.topic)

    decoded = json.loads(msg.payload)
    decoded["timestamp"] = int(decoded["timestamp"], 16)
    userdata.add_measurement(decoded)

def _on_status(client, userdata: we_s.MeasureDB, msg):
    #logger.verbose(msg.topic+" "+str(msg.payload))
    logger.debug("Receveived PUBLISH!")
    logger.debug("From: " + msg.topic)

    decoded = json.loads(msg.payload)
    decoded["timestamp"] = int(decoded["timestamp"], 16)
    decoded["voltage"] = decoded.pop("vcc")

    if decoded.get("name") is None:
        decoded["name"] = None
    if decoded.get("lastreset") is None:
        decoded["lastreset"] = None
    if decoded.get("rssi") is None:
        decoded["rssi"] = None
    userdata.add_status(decoded)

def _on_message(client, userdata, message): # pragma: no coverage
    logger.debug("Received message '" + str(message.payload) + "' on topic '"
        + message.topic + "' with QoS " + str(message.qos))

def mainmqtt(maindb: str, backup: Optional[str]=None, hostname="localhost", testing: bool=False, logqueue: Queue=None):
    if testing:
        logger.setLevel(logging.DEBUG)
    elif ( not logger.hasHandlers() ) and logqueue:
        handler = logging.handlers.QueueHandler(logqueue)
        logger.addHandler(handler)
    if maindb:
        if not backup:
            logger.warning("No backup database specified!")
        db = we_s.get_db(maindb, backup, logqueue=logqueue)
    else: # pragma: no coverage
        logger.warning("No main database specified.")
        logger.warning("Using standard arguments: DB = test.db, no backup!")
        db = we_s.get_db("test.db", logqueue=logqueue)

    listener1 = mqtt.Client(userdata=db)
    listener1.on_connect = _on_connect
    listener1.on_message = _on_message
    listener1.message_callback_add("/sensor/+/status", _on_status)
    listener1.message_callback_add("/sensor/+/mes", _on_measure)
    listener1.message_callback_add("/sensor/+/config", _on_config)
    self_process = threading.current_thread()
    while getattr(self_process, 'do_run', False):
        if not listener1.is_connected():
            try:
            #    Use this if running a MQTT server on your local machine:
            #   listener1.connect(host="localhost")
                _connerror_handler(listener=listener1, host=hostname)
            except: # pragma: no coverage
                logger.critical(f"Couldn't connect to host {hostname}.")
                logger.critical("As no connection was possible after 5 tries, the process is killed.")
                break
        else:
            sleep(1)
    logger.warning("Stopping MQTT Thread.")
    listener1.disconnect()
    listener1.loop_stop()
    if hasattr(db, "backup_thread"):
        db.backup_thread.do_run = False
    db = None
    del db
    logger.warning("MQTT thread stopped.")

def _connerror_handler(listener: mqtt.Client, host: str, **kwargs):
    try:
        listener.connect(host, **kwargs)
    except ConnectionError as e:  # pragma: no coverage
        logger.error(f"Error trying to connect to host: {e}")
        logger.error("Retrying 5 times.")
        i = 0
        while i < 5:
            sleep(2)
            logger.warning(f"Attempt {i+1}:")
            try:
                listener.reconnect()
            except ConnectionError as f: # pragma: no coverage
                logger.error(f"Failed: {f}")
                i+=1
            else:
                logger.warning("Succeded!")
                break
        if i >= 5:
            logger.critical("Failed to connect to MQTT host.")
            raise
    else:
        listener.loop_start()
        sleep(1)


if __name__ == "__main__": # pragma: no coverage
    logger.warning("weather_receive.py has been called directly.")
    logger.warning("Using standard arguments: DB = test.db, no backup, host = localhost")
    mainmqtt("test.db")
