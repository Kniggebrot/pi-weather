#!/usr/bin/env python3
"""
Start a backend for receiving measurements and sending stored ones via a REST API.
"""

import pathlib
import signal
from werkzeug.exceptions import HTTPException
from werkzeug.middleware.proxy_fix import ProxyFix
import os
import logging, logging.handlers
import threading
from flask import Flask, abort, current_app, redirect, render_template, url_for
import atexit

import weather_website.weather_store as we_s
import weather_website.debugger as dbg
from weather_website.setup_logging import setup_file_logging, setup_logging

# Setup Console formatting, with queue logger support for threads from requests etc.
listener, logqueue = setup_logging()

package_logger = logging.getLogger(__name__)
package_logger.setLevel(logging.INFO)

dbg.init_debugger_if_needed()
devel_secure_key = "dev"

def create_app(test_config=None):
    logger = package_logger
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        DB_WEATHER=pathlib.Path(app.instance_path, 'test.db'),
        BACKUP_WDB=pathlib.Path(app.instance_path, 'backup.db'),
        DB_USER=pathlib.Path(app.instance_path, "user.db"),
        BACKUP_UDB=pathlib.Path(app.instance_path, "backup_udb.db"),
        MQTTHOST="localhost",
        SECRET_KEY=devel_secure_key, # DO NOT FORGET TO REPLACE SECRET KEY WITH A SECURE KEY IN PRODUCTION!!!
    )

    if test_config or (os.getenv("FLASK_ENV") == "development"):
        app.config.from_mapping(test_config)
    else: # pragma: no coverage
        # No need to create an unused instance dir when testing
        try:
            os.makedirs(app.instance_path, exist_ok=True)
        except OSError:
            pass
        try:
            # Setup File logging
            listener.addHandler(setup_file_logging(app))
            app.config.from_pyfile("config.py") # Should replace config options from before
        except FileNotFoundError:
            logger.error(f"Did not find config.py under {app.instance_path}/config.py!")
            logger.warning("Assuming this is the first time an instance config.py is used; copying repo's config.py to instance path.")

            # Try to import a secret key from repo config.py to see if a key still needs to be generated
            try:
                import pkgutil
                from . import config
                if not config.SECRET_KEY:
                    raise AttributeError # Will be raised before anyway
            except (ImportError, AttributeError):
                logger.warning("No secret key found in repo config. Generating one...")
                import secrets
                data = pkgutil.get_data(__name__, "config.py").decode("utf-8")
                with pathlib.Path(app.instance_path, "config.py").open("x")  as instancecfg:
                    instancecfg.write(data)
                    instancecfg.write(f"SECRET_KEY='{secrets.token_hex()}'{os.linesep}")
            else:
                data = pkgutil.get_data(__name__, "config.py").decode("utf-8")
                with pathlib.Path(app.instance_path, "config.py").open("x") as instancecfg:
                    instancecfg.write(data)

            app.config.from_pyfile("config.py")
            app.config.from_prefixed_env() # Convenient way to set options in docker; e.g. FLASK_MQTTHOST = "mqtt"

    #################################################################################################

    @app.get("/")
    def index():
        db = we_s.get_db_flask()
        devicelist = db.get_devices()
        if isinstance(devicelist, list):
            lastinfo = db.get_dashboard(devicelist)
        else:
            lastinfo = []
        return render_template("dashboard.html", display=lastinfo)

    @app.get("/devices/<device_id>")
    def get_devicedetail(device_id):
        db = we_s.get_db_flask()
        info = db.get_fullstatus(device_id)

        # Error checking, or return proper page
        if info == []:
            abort(404)
        elif info == 400: # pragma: no coverage
            abort(400) # Bad request
        elif info == 500:
            return abort(500)
        
        return render_template("devicedetail.html", details=info[0], id=device_id)

    @app.get("/devices/<device_id>/")
    def forward2(device_id):
        return redirect(url_for("get_devicedetail", device_id=device_id), 301)

    @app.errorhandler(HTTPException)
    def errorpage(failure):
        return render_template("error.html", error=failure), failure.code
    
    we_s.init_flask(app)
    import weather_website.user_db as udb
    udb.init_udb_flask(app)

    import weather_website.api as api
    app.register_blueprint(api.bp_api)
    import weather_website.auth as auth
    app.register_blueprint(auth.bp_auth)

    if app.config.get("PROXIED"): # pragma: no coverage
        # True or None, if not set in config.py
        app.wsgi_app = ProxyFix(
            app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
        )
    
    app.logqueue = logqueue
    return app

def launch_app(forwarded_config=None):
    # Used to launch app via waitress
    app = create_app(forwarded_config)
    logger = package_logger
    
    with app.app_context():
        if current_app.config["SECRET_KEY"] == devel_secure_key and not current_app.testing:
            logger.error("Default secret key usage in production detected!")
            logger.error(f"Insert a proper secure key in {app.instance_path}/config.py !")
            logger.error("See https://flask.palletsprojects.com/en/2.1.x/tutorial/deploy/#configure-the-secret-key")
            raise Exception("Attempted usage of dev secret key in production")
        
        logger.info(f"Main weather DB: {current_app.config['DB_WEATHER']}")
        logger.info(f"Backup: {current_app.config['BACKUP_WDB']}")

        logger.info(f"Main user DB: {current_app.config['DB_USER']}")
        logger.info(f"Backup: {current_app.config['BACKUP_UDB']}")

        import weather_website.backend.weather_receive as we_r
        app.receive_thread = threading.Thread(target=we_r.mainmqtt, args=(current_app.config['DB_WEATHER'], current_app.config['BACKUP_WDB'], current_app.config['MQTTHOST'], False, app.logqueue), daemon=True) # only launching production through this!
        app.receive_thread.do_run = True
        app.receive_thread.start()

    def exit_mqtt(signum=None, stack=None): # pragma: no coverage
        app.receive_thread.do_run = False
        app.receive_thread.join(3)
    
    atexit.register(exit_mqtt)
    signal.signal(signal.SIGINT, exit_mqtt)
    signal.signal(signal.SIGTERM, exit_mqtt)
    return app

if __name__ == "__main__": # pragma: no coverage
    launch_app()
