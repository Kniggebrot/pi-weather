# Tests for User DB
import pathlib
from typing import Any
import pytest

from weather_website.user_db import UserDB, get_udb_flask


def test_init_udb(runner):
    result = runner.invoke(args=["init-user-db",])
    assert "User DB initialized.\n" in result.output

def test_set_admin(runner):
    result = runner.invoke(args=["set-admin", "test"])
    assert "Made user test an admin.\n" in result.output

    result2 = runner.invoke(args=["set-admin", "rando"])
    assert "User rando not found in DB!\n" in result2.output

def test_unset_admin(runner):
    result = runner.invoke(args=["unset-admin", "admin"])
    assert "Reset user admin.\n" in result.output

    result = runner.invoke(args=["unset-admin", "rando"])
    assert "User rando not found in DB!\n" in result.output

def test_no_memory():
    with pytest.raises(Exception, match="User DB needs to be in a file"):
        udb = UserDB(":memory:")

def test_with_backup(udb_instance):
    assert hasattr(udb_instance, "bckpath")
    assert hasattr(udb_instance, "backup_thread")

def test_get_db_flask(app):
    with app.app_context():
        db = get_udb_flask()
        assert isinstance(db, UserDB)
        assert db is get_udb_flask()
    
    with app.app_context():
        # New db connection opened for new context; db will not run __del__ until the function (or test) has run through
        assert db is not get_udb_flask()

@pytest.mark.slow
@pytest.mark.extraslow
def test_snapshot(blocking_udb_instance: tuple[UserDB, Any]):
    udb, blocker = blocking_udb_instance
    assert udb.get_userinfo("admin") # DB still available

    blocker.start()
    assert udb.add_user("failing","user") == "Internal Server Error"
    errorpath = udb.errordb
    assert errorpath != pathlib.Path(".") # Unset path == working dir
    if errorpath != pathlib.Path("."): # pragma: no branch
        errorpath.unlink()

@pytest.mark.parametrize(["user","passwd","output"], [
    ("admin", "admin", 1), 
    ("admin", "hackerman", "Invalid username or password"), 
    ("fakeuser", "fakepasswd", "Invalid username or password"),
    ("test", "test", 2)
])
def test_login(udb_instance: UserDB, user, passwd, output):
    assert udb_instance.check_user(user, passwd) == output

@pytest.mark.parametrize(["user","passwd","output"], [
    ("test2", "test2", None), 
    ("test3", "test3", "User already registered"), 
    ("admin", "tryit", "User already registered"),
    ("test", "test", "User already registered"),
    ("poe", "nevermore", None),
    ("user2", "tryit", None),
])
def test_add_user(udb_instance: UserDB, user, passwd, output):
    assert udb_instance.add_user(user, passwd) == output
    # OperationalError handling already handled in test_snapshot

@pytest.mark.parametrize(["user", "output"], [
    (None, "No identifier for user given"),
    (1, {"id":1, "username":"admin", "permissions": ["all"]}),
    (2, {"id":2, "username":"test", "permissions": ["send_downlinks","active"]}),
    (69, {"id": None, "permissions": [], "user": None}),
    ("test", {"id":2, "username":"test", "permissions": ["send_downlinks","active"]}),
    ("admin", {"id":1, "username":"admin", "permissions": ["all"]}),
    ("fakeuser", {"id": None, "permissions": [], "user": None}),
])
def test_get_userinfo(udb_instance, user, output):
    assert udb_instance.get_userinfo(user) == output

@pytest.mark.parametrize(["user", "editor", "perms", "output"], [
    ("test", "admin", ["manage_users", "send_downlinks", "active"], None),
    ("test", "test3", ["active",], None),
    ("test", "test3", ["manage_users", "active"], "Invalid permissions"),
    ("test", "test3", ["all",], "Invalid permissions"),
    ("test", "test", ["all",], "Invalid permissions"),
    ("test3", "test3", ["all",], "Invalid permissions"),
    ("test", "admin", ["all",], None),
    ("test", "admin", ["active",], None),
    ("admin", "test3", ["active",], "Cannot edit Admins"),
    ("random", "test3", ["active",], "User does not exist"),
    ("random", "random", ["active",], "Invalid permissions"),
    ("admin", "admin2", ["active",], "Cannot edit Admins"),
])
def test_edit_user(udb_instance: UserDB, user, editor, perms, output):
    udb_instance.add_user("test2", "test2")
    oldperms = udb_instance.get_userinfo(user).get("permissions")

    assert udb_instance.edit_user(editor, user, perms) == output
    if output == None:
        assert udb_instance.get_userinfo(user).get("permissions") == perms
    else:
        assert udb_instance.get_userinfo(user).get("permissions") == oldperms

@pytest.mark.parametrize(["lamb", "butcher", "output"], [
    ("admin", "test", "Lacking permissions to remove users."),
    ("test", "admin", None),
    ("test69", "admin", "Targeted user does not exist"),
    ("random", "admin", "Targeted user does not exist"),
    ("admin", "admin2", "Can't delete other Admins. Contact sysadmin"),
    ("test", "test", None)
])
def test_remove_user(udb_instance: UserDB, lamb, butcher, output):
    assert udb_instance.remove_user(butcher, lamb) == output

@pytest.mark.parametrize(["user", "device", "downlink", "output"], [
    ("admin", "0123456789ab", {"CTIME":30, "NAME": "Tester", "STIME": 300}, None),
    ("test", "0123456789ab", {"CTIME":30, "NAME": "Tester", "STIME": 300}, None),
    ("test2", "0123456789ab", {"CTIME":30, "NAME": "Tester", "STIME": 300}, "Invalid permissions"),
    ("admin", "0123456789ab", {"config": "garbage"}, "Invalid downlink"),
])
def test_add_downlink(udb_instance: UserDB, user, device, downlink, output):
    assert udb_instance.add_downlink(user, device, downlink) == output
