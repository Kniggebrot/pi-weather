# Tests for measurement/weather DB
import pathlib
from time import sleep
import weather_website.weather_store as ws
import pytest

def test_init_wdb(runner):
    result = runner.invoke(args="init-weather-db")
    assert "Weather DB initialized.\n" in result.output

def test_memory_db():
    db = ws.MeasureDB()
    assert db.dbpath == ":memory:"
    db.init_tables()
    assert db.add_measurement(measurement={"id":"123456789abc", "timestamp":1651338040, "temp":23.0, "pres":1022.7, "humi":42.2}) != 500
    testmeasure = db.get_measurements("123456789abc")
    assert len( testmeasure ) == 1
    assert isinstance(testmeasure, list)
    assert isinstance(testmeasure[0], dict)

def test_db_with_backup(mdb_instance):
    assert hasattr(mdb_instance, "backuppath")
    assert hasattr(mdb_instance, "backup_thread")

def test_get_db(tmp_path):
    db = ws.get_db(tmp_path / "testest.db")
    assert isinstance(db, ws.MeasureDB)

def test_get_db_flask(app):
    with app.app_context():
        db = ws.get_db_flask()
        assert isinstance(db, ws.MeasureDB)
        assert db is ws.get_db_flask()
    
    with app.app_context():
        # New db connection opened for new context; db will not run __del__ until the function (or test) has run through
        assert db is not ws.get_db_flask()

def test_with_statements(tmp_path):
    with ws.MeasureDB(tmp_path / "testest.db") as testdb:
        # Nothing special to test, since that is done in other tests...
        assert isinstance(testdb, ws.MeasureDB)

    with ws.MeasureDB() as testdb:
        assert isinstance(testdb, ws.MeasureDB)

    with ws.MeasureDB() as testdb:
        assert isinstance(testdb, ws.MeasureDB)
        testdb.init_tables()
        testdb._database.isolation_level = "EXCLUSIVE"
        testdb._database.execute('''begin exclusive;''')
        testdb._database.execute('''insert into measurements values ("0123456789ab", 1651338040, 23.0, 1022.7, 42.2);''')

# Quite slow
@pytest.mark.slow
def test_failed_access_devices(blocking_mdb_instance):
    mdb_instance, blocker = blocking_mdb_instance
    # Second: Failure due to closed db
    blocker.start()
    sleep(1)
    # The db should be blocked now. So OperationalErrors should be raised internally when trying to read from DB without the commited insert
    assert mdb_instance.get_devices() == 500    

def test_invalid_measures(mdb_instance):
    # First: Invalid device parameters for get_* functions
    assert mdb_instance.get_measurements(False) == 400
    assert mdb_instance.get_measurements_time(False) == 400

# Quite slow
@pytest.mark.slow
def test_failed_access_measures(blocking_mdb_instance):
    mdb_instance, blocker = blocking_mdb_instance
    # Second: Failure due to closed db
    blocker.start()
    sleep(1)
    # The db should be blocked now. So OperationalErrors should be raised internally when trying to read from DB without the commited insert
    assert mdb_instance.get_measurements("0123456789ab") == 500    
    assert mdb_instance.get_measurements_time("0123456789ab") == 500    

def test_invalid_status(mdb_instance):
    # First: Invalid device parameters for get_* functions
    assert mdb_instance.get_status(False) == 400
    assert mdb_instance.get_status_time(False) == 400

# Quite slow
@pytest.mark.slow
def test_failed_access_status(blocking_mdb_instance):
    mdb_instance, blocker = blocking_mdb_instance
    # Second: Failure due to closed db
    assert mdb_instance.get_status("0123456789ab") != 500 # DB should still be accessible
    blocker.start()
    sleep(1)
    # The db should be blocked now. So OperationalErrors should be raised when trying to read from DB without the commited insert
    assert mdb_instance.get_status("0123456789ab") == 500
    assert mdb_instance.get_status_time("0123456789ab") == 500

def test_invalid_fullstatus(mdb_instance):
    # First: Invalid device parameters for get_* functions
    assert mdb_instance.get_fullstatus(False) == 400
    assert mdb_instance.get_fullstatus_time(False) == 400

# Quite slow
@pytest.mark.slow
def test_failed_access_fullstatus(blocking_mdb_instance):
    mdb_instance, blocker = blocking_mdb_instance
    # Second: Failure due to closed db
    assert mdb_instance.get_fullstatus("0123456789ab") != 500 # DB should still be available
    blocker.start()
    sleep(1)
    # The db should be blocked now. So OperationalErrors should be raised when trying to read from DB without the commited insert
    assert mdb_instance.get_fullstatus("0123456789ab") == 500
    assert mdb_instance.get_fullstatus_time("0123456789ab") == 500

# Extremely slow.
@pytest.mark.slow
@pytest.mark.extraslow
def test_emergency_restore(blocking_mdb_instance):
    mdb_instance, blocker = blocking_mdb_instance

    assert mdb_instance.add_measurement(measurement={"id":"0123456789ab", "timestamp":1651338040, "temp":23.0, "pres":1022.7, "humi":42.2}) != 500 # db still available
    # Test emergency restore
    blocker.start()
    try:
        assert mdb_instance.add_measurement(measurement={"id":"0123456789ab", "timestamp":1651338040, "temp":23.0, "pres":1022.7, "humi":42.2}) == None # now its locked
        errorpath = mdb_instance.errordb
        assert errorpath != pathlib.Path(".") # Empty path becomes pwdir
    finally:
        if errorpath != pathlib.Path("."): # pragma: no branch
            # Would only branch if test failed, which it shouldn't
            errorpath.unlink()
