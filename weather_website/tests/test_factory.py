from weather_website import create_app, launch_app
import pytest

def test_config():
    assert not create_app().testing
    assert create_app({"TESTING": True}).testing

def test_index(client):
    response = client.get("/")
    assert b"<title>Pi-Weather - Dashboard</title>" in response.data
    assert b'<h2 class="text-center">\n            <a href="/devices/0123456789ab" class="link-dark">\n                Test1\n            </a>\n        </h2>' in response.data

@pytest.mark.slow
def test_empty_index(blocking_app):
    app, blockers = blocking_app

    client = app.test_client()
    blockers[0].start()

    response = client.get("/")
    assert b"<title>Pi-Weather - Dashboard</title>" in response.data
    assert not (b'<h2 class="text-center">\n            <a href="/devices/0123456789ab" class="link-dark">\n                Test1\n            </a>\n        </h2>' in response.data)

def test_devicepage(client):
    response = client.get('/devices/0123456789ab')

    assert b"Measurements in the last 24 hours" in response.data
    assert b'<h2 class=\"text-center text-dark\">Test1</h2>' in response.data
    assert b'<h4 class=\"text-center text-secondary\">0123456789ab</h4>' in response.data
    assert b'<p id=\"reset_str\">Last reset due to ESP_RST_POWERON on 1651338040</p>' in response.data

@pytest.mark.slow
def test_faulty_devicepage(blocking_app):
    app, blockers = blocking_app

    client = app.test_client()

    response = client.get("/devices/fakedevice")
    assert not (b'<h2 class=\"text-center\">\n        <a href=\"/devices/0123456789ab\" class=\"link-dark\">\n            Test1\n        </a>\n    </h2>' in response.data)
    assert b'<h1 class="mb-2">404</h1>' in response.data

#   doesn't work, device url inputs seem to be sanitized
#   response = client.get("/devices/\x01\x00")
#   assert not (b'<h2 class=\"text-center\">\n        <a href=\"/devices/0123456789ab\" class=\"link-dark\">\n            Test1\n        </a>\n    </h2>' in response.data)
#   assert b'<h1 class="mb-2">400</h1>' in response.data

    blockers[0].start()

    response = client.get("/devices/0123456789ab")
    assert not (b'<h2 class="text-center">\n            <a href="/devices/0123456789ab" class="link-dark">\n                Test1\n            </a>\n        </h2>' in response.data)
    assert b'<h1 class="mb-2">500</h1>' in response.data

def test_errorpage(client):
    response = client.get("/404")

    assert response.status_code == 404
    assert b'<h1 class="mb-2">404</h1>' in response.data

def test_redirects(client):
    response = client.get("/api/devices", follow_redirects=True)
    assert len(response.history) == 1
    assert response.request.path == '/api/devices/'

    response2 = client.get("/devices/0123456789ab/", follow_redirects=True)
    assert len(response2.history) == 1
    assert response2.request.path == '/devices/0123456789ab'

def test_production():
    with pytest.raises(Exception, match="Attempted usage of dev secret key in production"):
        assert not launch_app({"SECRET_KEY": "dev"}).testing

    # Provided a valid config file is present in instance directory
    try:
        app = launch_app()
        assert not app.testing
    except Exception as e: # pragma: no coverage
        print(f"An error happened! {e}")
        print("Have you put a valid config file in your instance dir?")
        raise
    finally:
        try:
            app.receive_thread.do_run = False
            app.receive_thread.join()
        except Exception as e: # pragma: no coverage
            print(f"Teardown of MQTT thread in {__name__} failed: {e}")
