# Testing the authentification API and pages
from flask import session
import pytest

@pytest.mark.parametrize(["user", "password", "output"], [
    ("test", "test", b"Logged in successfully."),
    ("admin", "admin", b"Logged in successfully."),
    ("admin", "fakepwd", b"Invalid username or password"),
    ("test3", "admin", b"Invalid username or password"),
    ("test3", "test", b"Logged in successfully."),
    ("", "admin", b"Invalid username or password"),
    ("test3", "", b"Invalid username or password"),
    ("", "", b"Invalid username or password"),
])
def test_login(client, user, password, output):
    response = client.get("/user/login")
    assert True # "Login" heading in page
    assert response.status_code == 200
    
    with client:
        response = client.post("/user/login", data={"username": user, "password": password}, follow_redirects=True)
        assert output in response.data

        if response == b"Logged in successfully.":
            assert session["user_id"]

def test_no_repeated_logins(client):
    with client:
        client.post("/user/login", data={"username": "admin", "password": "admin"})
        old_id = session["user_id"]
        assert old_id

        client.post("/user/login", data={"username": "admin3", "password": "test"})
        assert old_id == session["user_id"]

@pytest.mark.parametrize(["user", "password", "output", "redirected_to"], [
    ("test2", "somepass", "Registered successfully.", "/user/login"),
    ("admin", "passwd", "User already registered", "/user/register"),
    ("regular_user", "admin", "Registered successfully.", "/user/login"),
    ("invalid1", "", "Password can't be empty.", "/user/register"),
    ("", "invalid2", "Username can't be empty.", "/user/register"),
])
def test_register(client, user, password, output, redirected_to):
    try:
        from html import unescape
    except ImportError:
        from html.parser import HTMLParser
        unescape = HTMLParser().unescape
    response = client.post("/user/register", data={"username": user, "password": password}, follow_redirects=True)
    assert output in unescape(response.text)
    assert response.request.path == redirected_to

@pytest.mark.parametrize(["id", "user", "passwd"], [
    (1, "admin", "admin"),
    (2, "test", "test"),
    (3, "test3", "test"),
])
def test_logout(client, id, user, passwd):
    with client:
        client.post("/user/login", data={"username": user, "password": passwd})
        assert session["user_id"] == id
        client.post("/user/logout", follow_redirects=True)
        assert session.get("user_id") == None
    
@pytest.mark.parametrize(["user","output", "code"], [
    ("admin", "Profile: admin", 200),
    ("test", "Profile: test", 200),
    ("unknownuser", "User not found", 404),
    ("", "Profile: admin", 200),
])
def test_profile(client, user, output, code):
    with client.session_transaction() as session:
        session["user_id"] = 1

    response = client.get(f"/user/{user}", follow_redirects=True)
    assert output in response.text
    assert response.status_code == code

@pytest.mark.parametrize(["id", "returned", "finalpage"], [
    (1, "User management", "/user/manage"), # Admin
    (2, "Lacking permissions to manage users", "/user/test/"), # Test
    (3, "User management", "/user/manage"), # Test3
])
def test_manage(client, id, returned, finalpage):
    with client.session_transaction() as session:
        session["user_id"] = id

    response = client.get("/user/manage", follow_redirects=True)
    assert returned in response.text
    assert response.request.path == finalpage

@pytest.mark.parametrize(["id", "editing", "answer"], [
    (1, {"username": "test", "permissions": {"all": True}}, "Permissions for test updated."),
    (2, {"username": "test3", "permissions": {"active": True}}, "Invalid permissions"),
    (3, {"username": "admin", "permissions": {}}, "Cannot edit Admins"),
    (3, {"username": "test", "permissions": {"manage_users": True, "allowedCheckbox": True}}, "Invalid permissions"),
    (3, {"username": "test", "permissions": {"active": True}}, "Permissions for test updated."),
    (3, {"username": "test", "permissions": {"active": True, "send_downlinks": True}}, "Permissions for test updated."),
    (3, {"username": "fakeuser", "permissions": {"active": True}}, "User does not exist"),
    (1, {"username": "admin2", "permissions": {"active": True}}, "Cannot edit Admins"),
])
def test_user_editing(client, id, editing: dict, answer):
    with client.session_transaction() as session:
        session["user_id"] = id

    response = client.put(f"/user/{editing['username']}", data=editing["permissions"], follow_redirects=True)
    assert answer in response.text

@pytest.mark.parametrize(["id", "deleted", "answer"], [
    (1, "test", "Deleted test successfully."),
    (1, "unknown", "Targeted user does not exist"),
    (3, "test", "Lacking permissions to remove other users."),
    (2, "test", "Deleted test successfully."),
])
def test_user_deleting(client, id, deleted, answer):
    with client.session_transaction() as session:
        session["user_id"] = id

    response = client.delete(f"/user/{deleted}", follow_redirects=True)
    assert answer in response.text
