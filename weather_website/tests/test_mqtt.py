from socket import gaierror
import threading
from flask import current_app
import paho.mqtt.client as mqtt
from time import sleep
from weather_website.backend.weather_receive import mainmqtt

import pytest

@pytest.mark.slow
@pytest.mark.extraslow
@pytest.mark.needs_mqttserver
def test_recv_status(mqtt_client_plus_flask):
    mqtt_client, client = mqtt_client_plus_flask
    message = mqtt_client.publish("/sensor/deadbeefdead/status", '{"id":"deadbeefdead","timestamp":"611d470b","chiptemp":35.0,"vcc":4460, "rssi":-50}', qos=1)
    message.wait_for_publish()
    assert(message.rc == mqtt.MQTT_ERR_SUCCESS)
    sleep(1) # sometimes, the client is faster than the mqtt callback...
    resindb = client.get("/api/devices/deadbeefdead/status?count=1")
    resindb = resindb.json
    assert resindb.get("data")
    resindb = resindb["data"]
    assert len(resindb) == 1
    assert not resindb[0].get("name")
    assert resindb[0].get("chiptemp") == 35.0

@pytest.mark.slow
@pytest.mark.extraslow
@pytest.mark.needs_mqttserver
def test_recv_fullstatus(mqtt_client_plus_flask):
    mqtt_client, client = mqtt_client_plus_flask
    message = mqtt_client.publish("/sensor/deadbeefdead/status", '{"id":"deadbeefdead","timestamp":"611d4701","lastreset":"ESP_RST_BROWNOUT","chiptemp":35.0,"vcc":4384,"name":"Tisch"}', qos=1)
    message.wait_for_publish()
    assert(message.rc == mqtt.MQTT_ERR_SUCCESS)
    sleep(1) # sometimes, the client is faster than the mqtt callback...
    resindb = client.get("/api/devices/deadbeefdead/status?count=1&full=1").json
    assert resindb.get("data")
    resindb = resindb["data"]
    assert len(resindb) == 1
    assert resindb[0].get("name") == "Tisch"
    assert resindb[0].get("lastreset") == "ESP_RST_BROWNOUT"

@pytest.mark.slow
@pytest.mark.extraslow
@pytest.mark.needs_mqttserver
def test_recv_mes(mqtt_client_plus_flask):
    mqtt_client, client = mqtt_client_plus_flask
    message = mqtt_client.publish("/sensor/deadbeefdead/mes", '{"id":"deadbeefdead","timestamp":"611d46df","temp":22.6,"pres":1008.8,"humi":59.3}')
    message.wait_for_publish()
    assert(message.rc == mqtt.MQTT_ERR_SUCCESS)
    sleep(1) # sometimes, the client is faster than the mqtt callback...
    resindb = client.get("/api/devices/deadbeefdead/measure?count=1").json
    assert resindb.get("data")
    resindb = resindb["data"]
    assert len(resindb) == 1
    assert resindb[0].get("temperature") == 22.6
    assert resindb[0].get("pressure") == 1008.8

@pytest.mark.slow
@pytest.mark.extraslow
@pytest.mark.needs_mqttserver
def test_mqtt_without_backup(app):
    mqtt_client = mqtt.Client("fake-esp")
    with app.app_context():
        try:
            mqtt_client.connect("mqtt")
            main_thread = threading.Thread(target=mainmqtt, args=(current_app.config["DB_WEATHER"], None, "mqtt", True))
        except gaierror:
            mqtt_client.connect("localhost")
            main_thread = threading.Thread(target=mainmqtt, args=(current_app.config["DB_WEATHER"], None, "localhost", True))
        main_thread.do_run = True
        main_thread.start()

    mqtt_client.loop_start()
    sleep(10)

    message = mqtt_client.publish("/sensor/deadbeefdead/mes", '{"id":"deadbeefdead","timestamp":"611d46df","temp":22.6,"pres":1008.8,"humi":59.3}')
    message.wait_for_publish()
    assert (message.rc == mqtt.MQTT_ERR_SUCCESS)
    sleep(1) # sometimes, the client is faster than the mqtt callback...
    client = app.test_client()
    resindb = client.get("/api/devices/deadbeefdead/measure?count=1").json
    assert resindb.get("data")
    resindb = resindb["data"]
    assert len(resindb) == 1
    assert resindb[0].get("temperature") == 22.6
    assert resindb[0].get("pressure") == 1008.8

    main_thread.do_run = False
    main_thread.join()
    try:
        assert main_thread.is_alive
    finally:
        mqtt_client.disconnect()
        mqtt_client.loop_stop()
