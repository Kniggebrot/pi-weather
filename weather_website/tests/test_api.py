import pytest
from time import time

def test_devices(client):
    response = client.get("/api/devices/")
    data = response.json["data"][0]
    assert data["id"] == "0123456789ab"
    assert data["name"] == "Test1"

def test_get_measure(client):
    response = client.get('/api/devices/0123456789ab/measure?count=1')
    data = response.json["data"][0]
    assert data["humidity"] == 42.2
    assert data["pressure"] == 1022.7
    assert data["temperature"] == 23.0
    assert data["time"] == 1651338040

    response = client.get('/api/devices/missing/measure')
    assert response.status_code == 404
    assert response.json["error"] == 'Device or measurements not found'

    response = client.get('/api/devices/0123456789ab/measure?fromts=1651338039&tots=1651338041')
    data = response.json["data"][0]
    assert data["humidity"] == 42.2
    assert data["pressure"] == 1022.7
    assert data["temperature"] == 23.0
    assert data["time"] == 1651338040

    response = client.get(f'/api/devices/0123456789ab/measure?fromts=1651338039&tots={time()+100}')
    data = response.json["data"][0]
    assert data["humidity"] == 42.2
    assert data["pressure"] == 1022.7
    assert data["temperature"] == 23.0
    assert data["time"] == 1651338040

    response = client.get(f'/api/devices/0123456789ab/measure?fromts={time()+100}&tots=1651338041')
    assert response.status_code == 404
    assert response.json["error"] == 'Device or measurements not found'

    response = client.get('/api/devices/0123456789ab/measure?fromts=1651338000&tots=1651338001')
    assert response.status_code == 404
    assert response.json["error"] == 'Device or measurements not found'

def test_get_status(client):
    response = client.get('/api/devices/0123456789ab/status?count=1', follow_redirects=True)
    assert response.status_code == 200

    assert len(response.json["data"]) == 1
    
    content = response.json.get("data")[0]
    assert content["name"] == None
    assert content["lastreset"] == None
    assert content["chiptemp"] == 35.0
    assert content["rssi"] == -50
    assert content["time"] == 1651338050
    assert content["voltage"] == 3130

    response = client.get('/api/devices/0123456789ab/status?fromts=1651338039&tots=1651338051')
    assert response.status_code == 200
    assert len(response.json["data"]) == 2

    response = client.get('/api/devices/0123456789ab/status?full=1&fromts=1651338039&tots=1651338051')
    assert response.status_code == 200
    assert len(response.json["data"]) == 1

    response = client.get(f'/api/devices/0123456789ab/status?fromts=1651338039&tots={time()+100}')
    assert response.status_code == 200
    assert len(response.json["data"]) == 2

    response = client.get(f'/api/devices/0123456789ab/status?full=1&fromts={time()+100}&tots=1651338051')
    assert response.status_code == 404
    assert response.json["error"] == 'Device or messages not found'

    response = client.get('/api/devices/0123456789ab/status?full=1&count=1')
    assert response.status_code == 200
    assert len(response.json["data"]) == 1

    content = response.json["data"][0]
    assert content["name"] == "Test1"
    assert content["lastreset"] == "ESP_RST_POWERON"
    assert content["chiptemp"] == 35.1
    assert content["rssi"] == -51
    assert content["time"] == 1651338040
    assert content["voltage"] == 3129

@pytest.mark.slow
def test_fail_measure(blocking_app):
    app, blockers = blocking_app

    client = app.test_client()
    blockers[0].start()

    response = client.get('/api/devices/0123456789ab/measure?count=1')
    assert not response.json.get("data")
    assert response.json["error"] == "Server error during data fetch"

@pytest.mark.slow
def test_fail_status(blocking_app):
    app, blockers = blocking_app

    client = app.test_client()
    blockers[0].start()

    response = client.get('/api/devices/0123456789ab/status?count=1')
    assert not response.json.get("data")
    assert response.json["error"] == "Server error during data fetch"
