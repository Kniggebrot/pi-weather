insert into user (username, passwd, permissions) VALUES ('admin', 'pbkdf2:sha256:260000$ml4HG3RDsjpi695V$48294b900eae0d504b8301f0a82302fd5b7d5c0b14b3a9ba801e80466d9af7ab','{"permissions":["all"]}'),
    ('test', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f','{"permissions":["send_downlinks","active"]}'),
    ('test3', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f','{"permissions":["manage_users","active"]}'),
    ('admin2', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f','{"permissions":["all"]}');
INSERT INTO downlinks (user_id, device_id, downlink, sent) VALUES 
    (1,"0123456789",'{"data":{"CTIME": 15}}', 1651338041);
