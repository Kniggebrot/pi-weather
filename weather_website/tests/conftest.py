import atexit
import contextlib
import os
from socket import gaierror
import sqlite3
import tempfile
import threading
from time import sleep
from flask import current_app
import paho.mqtt.client as mqtt

import pytest
from weather_website import create_app
from weather_website.user_db import UserDB, get_udb_flask
from weather_website.weather_store import MeasureDB, init_db
from weather_website.backend.weather_receive import mainmqtt

with open(os.path.join(os.path.dirname(__file__), 'test_mdb.sql'), 'rb') as f:
    _test_sql_mdb = f.read().decode('utf8')

with open(os.path.join(os.path.dirname(__file__), 'test_udb.sql'), 'rb') as f:
    _test_sql_udb = f.read().decode('utf8')

@pytest.fixture
def app():   
    test_fd, test_path = tempfile.mkstemp()
    backup_fd, backup_path = tempfile.mkstemp()

    test_ufd, test_upath = tempfile.mkstemp()
    backup_ufd, backup_upath = tempfile.mkstemp()

    app = create_app({
        "TESTING": True,
        "DB_WEATHER": test_path,
        "BACKUP_WDB": backup_path,
        "DB_USER": test_upath,
        "BACKUP_UDB": backup_upath,
    })

    with app.app_context():
        init_db()
        database = sqlite3.connect(current_app.config["DB_WEATHER"])
        database.executescript(_test_sql_mdb)
        database.commit()
        database.close()
        # Same for user db
        get_udb_flask().init_db()
        database = sqlite3.connect(current_app.config["DB_USER"])
        database.executescript(_test_sql_udb)
        database.commit()
        database.close()

    yield app

    os.close(test_fd)
    os.close(backup_fd)
    os.close(test_ufd)
    os.close(backup_ufd)
    os.unlink(test_path)
    os.unlink(backup_path)
    os.unlink(test_upath)
    os.unlink(backup_upath)

@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()

@pytest.fixture
def mdb_instance():
    test_fd, test_path = tempfile.mkstemp()
    backup_fd, backup_path = tempfile.mkstemp()

    mdb = MeasureDB(test_path, backup_path, 1, 10)
    mdb.init_tables()
    yield mdb
    
    mdb.backup_thread.do_run = False
    mdb.backup_thread.join(10)
    del mdb

    os.close(test_fd)
    os.close(backup_fd)
    os.unlink(test_path)
    os.unlink(backup_path)

@pytest.fixture
def udb_instance():
    test_fd, test_path = tempfile.mkstemp()
    backup_fd, backup_path = tempfile.mkstemp()

    udb = UserDB(test_path, backup_path, 1, 10)
    udb.init_db()
    with contextlib.closing(sqlite3.connect(test_path)) as filler:
        filler.executescript(_test_sql_udb)
        filler.commit()
    yield udb
    
    udb.backup_thread.do_run = False
    udb.backup_thread.join(10)
    del udb

    os.close(test_fd)
    os.close(backup_fd)
    os.unlink(test_path)
    os.unlink(backup_path)

@pytest.fixture
def blocking_app(app):
    def block_db(dbpath, script='''begin exclusive;
            insert into measurements values ("0123456789ab", 1651338040, 23.0, 1022.7, 42.2);'''):
        blocker = sqlite3.connect(dbpath, isolation_level="EXCLUSIVE")
        try:
            blocker.commit()
            blocker.executescript(script)
            sleep(3)
        finally:
            blocker.rollback()
            blocker.close()

    blockers: list[threading.Thread]=[]
    with app.app_context():
        blockers.append(threading.Thread(target=block_db, args=(current_app.config["DB_WEATHER"],)))
        blockers.append(threading.Thread(target=block_db, args=(current_app.config["DB_USER"],'''begin exclusive;
            INSERT INTO downlinks (user_id, device_id, downlink, sent) VALUES 
            (1,"0123456789","{"data":{"config": "trivago"}}", 1651338041) 
        ''')))
    
    yield (app, blockers)

    for blocker in blockers:
        try:
            blocker.join()
        except RuntimeError: # pragma: no coverage
            # Thread was never started...
            pass

@pytest.fixture
def blocking_mdb_instance(mdb_instance: MeasureDB):
    def block_db(dbpath):
        blocker = sqlite3.connect(dbpath, isolation_level="EXCLUSIVE")
        try:
            blocker.commit()
            blocker.executescript('''begin exclusive;
            insert into measurements values ("0123456789ab", 1651338040, 23.0, 1022.7, 42.2);''')
            sleep(7)
        finally:
            blocker.rollback()
            blocker.close()

    blocker_thread=threading.Thread(target=block_db, args=(mdb_instance.dbpath,))
    
    yield (mdb_instance, blocker_thread)

    try:
        blocker_thread.join()
    except RuntimeError: # pragma: no coverage
        # Thread was never started...
        pass

@pytest.fixture
def blocking_udb_instance(udb_instance: UserDB):
    def block_db(dbpath):
        blocker = sqlite3.connect(dbpath, isolation_level="EXCLUSIVE")
        try:
            blocker.commit()
            blocker.executescript('''begin exclusive;
            INSERT INTO downlinks (user_id, device_id, downlink, sent) VALUES 
            (1,"0123456789",'{"data":{"config": "trivago"}}', 1651338041)''')
            sleep(7)
        finally:
            blocker.rollback()
            blocker.close()

    blocker_thread=threading.Thread(target=block_db, args=(udb_instance.dbpath,))
    
    yield (udb_instance, blocker_thread)

    try:
        blocker_thread.join()
    except RuntimeError: # pragma: no coverage
        # Thread was never started...
        pass

@pytest.fixture
def mqtt_client_plus_flask(app):
    fake_esp = mqtt.Client(client_id="fake_esp")
    with app.app_context():
        try:
            fake_esp.connect("mqtt")
            main_thread = threading.Thread(target=mainmqtt, args=(current_app.config["DB_WEATHER"], current_app.config["BACKUP_WDB"], "mqtt", True), daemon=True)
        except gaierror:
            fake_esp.connect("localhost")
            main_thread = threading.Thread(target=mainmqtt, args=(current_app.config["DB_WEATHER"], current_app.config["BACKUP_WDB"], "localhost",True), daemon=True)
        main_thread.do_run = True
        main_thread.start()
    
    def wait_for_mainmqtt():
        main_thread.do_run = False
        main_thread.join()

    atexit.register(wait_for_mainmqtt)
    fake_esp.loop_start()
    sleep(10)
    yield (fake_esp, app.test_client())

    fake_esp.disconnect()
    fake_esp.loop_stop()

    wait_for_mainmqtt()
