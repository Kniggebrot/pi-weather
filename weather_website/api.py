from flask import Blueprint, redirect, jsonify, request, url_for
import time

import weather_website.weather_store as we_s

bp_api = Blueprint("api", __name__, url_prefix="/api")

"""
'REST' API: (all responses JSON)
v1
GET     /api/devices/                           Get list of devices

GET     /api/devices/<device_id>/measure        Get measurements of device, either the last n measurements (count) or all of them in a set timeframe (fromts ... tots)
GET     /api/devices/<device_id>/status         Get status(es) of device, either the last n statuses (count) or all of them in a set timeframe (fromts ... tots)
GET     /api/devices/<device_id>/config         Get config of said device (Collection & Send Period, Name)

GET     /api/devices/<device_id>/downlink       Get (scheduled) downlinks for device
POST    /api/devices/<device_id>/downlink       Publish a new config via MQTT to the device when it is connected and sends its next status
DELETE  /api/devices/<device_id>/downlink/<id>  Delete scheduled downlink
"""

@bp_api.get("/devices")
def forward():
    return redirect(url_for(".get_devicelist"), 301)

@bp_api.get("/devices/")
def get_devicelist():   # Return a list of device ids with names (if set)
    db = we_s.get_db_flask()
    dlist = db.get_devices()
    return jsonify(data=dlist)

#################################################################################################

@bp_api.get("/devices/<device_id>/measure")
def get_measure(device_id):
    mlist = []
    # Number of requested statuses specified?
    if request.args.get("count"):
        count = request.args.get("count", 1, type=int)
        #print(f"count={count}")
        db = we_s.get_db_flask()
        mlist = db.get_measurements(device_id, count=count)
    else:
        # Get measurements in timeframe, either last 24 hours or set one
        from_time = request.args.get("fromts", time.time()-86400, type=float)
        to_time   = request.args.get("tots", time.time(), type=float)
        
        # Check from time is lower than to time, and both are lower than current time
        if to_time > time.time():
            to_time = time.time()
        if from_time > time.time():
            from_time = time.time()
        if from_time > to_time:
            old_from = from_time
            from_time = to_time
            to_time = old_from

        from_time = int(from_time)
        to_time = int(to_time)

        db = we_s.get_db_flask()
        mlist = db.get_measurements_time(device_id, time_lower=from_time, time_higher=to_time)


    if mlist == []:
        return ({"error": "Device or measurements not found"}, 404)
    elif mlist == 400: # pragma: no coverage
        return ({"error": "Bad ID entered"}, 400)
    elif mlist == 500:
        return ({"error": "Server error during data fetch"}, 500)
    else:
        return jsonify(data=mlist)

@bp_api.get("/devices/<device_id>/status")
def get_status(device_id):
    slist = []
    # Number of requested statuses specified?
    if request.args.get("count"):
        cnt = request.args.get("count", 1, type=int)
        fullstatus = request.args.get("full", 0, type=int)
        #print(f"count={cnt}, full={fullstatus}")
        db = we_s.get_db_flask()
        if not fullstatus:
            slist = db.get_status(device_id, count=cnt)
        else:
            slist = db.get_fullstatus(device_id, count=cnt)
    
    else:   # Get statuses in timeframe, either last 24 hours or set one
        from_time = request.args.get("fromts", time.time()-86400, type=float)
        to_time   = request.args.get("tots", time.time(), type=float)
        
        # Check from time is lower than to time, and both are lower than current time
        if to_time > time.time():
            to_time = time.time()
        if from_time > time.time():
            from_time = time.time()
        if from_time > to_time:
            old_from = from_time
            from_time = to_time
            to_time = old_from

        from_time = int(from_time)
        to_time = int(to_time)

        fullstatus = request.args.get("full", 0, type=int)
        #print(f"count={cnt}, full={fullstatus}")
        db = we_s.get_db_flask()
        if not fullstatus:
            slist = db.get_status_time(device_id, time_lower=from_time, time_higher=to_time)
        else:
            slist = db.get_fullstatus_time(device_id, time_lower=from_time, time_higher=to_time)
        
    # Error checking, or return proper data
    if slist == []:
        return ({"error": "Device or messages not found"}, 404)
    elif slist == 400: # pragma: no coverage
        return ({"error": "Bad ID entered"}, 400)
    elif slist == 500:
        return ({"error": "Server error during data fetch"}, 500)
    else:
        return jsonify(data=slist)

@bp_api.get("/devices/<device_id>/config")
def get_config(device_id):
    db = we_s.get_db_flask()
    cfg = db.get_config(device_id)
    if cfg == {}:
        return ({"error": "Device config not found"}, 404)
    elif cfg == 400: # pragma: no coverage
        return ({"error": "Bad ID entered"}, 400)
    elif cfg == 500:
        return ({"error": "Server error during data fetch"}, 500)
    return jsonify(config=cfg)


#################################################################################################

@bp_api.get("/devices/<device_id>/downlink")
def get_downlinks(device_id):
    pass

@bp_api.post("/device/<device_id>/downlink")
def queue_downlink(device_id):
    pass

@bp_api.delete("/device/<device_id>/downlink/<dl_id>")
def delete_downlink(device_id, dl_id):
    pass
