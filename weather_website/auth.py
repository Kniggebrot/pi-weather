import functools
from flask import abort, Blueprint, flash, g, redirect, render_template, request, session, url_for

import weather_website.user_db as udb

bp_auth = Blueprint("auth", __name__, url_prefix="/user", template_folder="templates/auth")
# Authentification blueprint, to verify user has rights to see/edit a device's config.

def check_user(view):
    @functools.wraps(view)
    def wrapped_view(*args, **kwargs):
        if g.user is None:
            return redirect(url_for(".login"))
        
        return view(*args, **kwargs)

    return wrapped_view

@bp_auth.route("/login", methods=("GET", "POST"))
def login():
    if g.user:
        return redirect(url_for(".profile", username=g.user["username"]))
    if request.method == "POST":
        db = udb.get_udb_flask()
        user = request.form.get("username", str)
        passwd = request.form.get("password", str)

        id = db.check_user(user, passwd)
        if not isinstance(id, int):
            flash(id, "error")
        else:
            session.clear()
            session["user_id"] = id
            flash("Logged in successfully.", "info")
            if request.referrer:
                return redirect(request.referrer)
            else:
                return redirect(url_for(".profile", username=user))
    return render_template("login.html")
    
@bp_auth.route("/register", methods=("GET", "POST"))
def register():
    if request.method == "POST":
        user = request.form["username"]
        passwd = request.form["password"]
        msg = None

        if not user:
            msg = "Username can't be empty."
        if not passwd:
            msg = "Password can't be empty."

        if not msg:
            db = udb.get_udb_flask()
            msg = db.add_user(user, passwd)
            if not msg:
                flash("Registered successfully.", "info")
                return redirect(url_for(".login"), 303)
        flash(msg, "error")

    return render_template("auth/register.html")


@bp_auth.post("/logout")
def logout():
    session.clear()
    return redirect(url_for("index"), 303)

@bp_auth.get("/<username>/")
@check_user
def profile(username):
    if not username:
        return abort(404, "User not found")
    db = udb.get_udb_flask()
    userinfo = db.get_userinfo(username)
    
    if userinfo.get("id"):
        return render_template("profile.html", info=userinfo)
    return abort(404, "User not found")

@bp_auth.get("/")
def redirect_empty_username():
    return redirect(url_for(".login"))

@bp_auth.get("/manage")
@check_user
def user_manager():
    if not ( ("manage_users" in g.user["permissions"]) or ("all" in g.user["permissions"]) ):
        flash("Lacking permissions to manage users", "error")
        if not request.referrer:
            return redirect(url_for(".profile", username=g.user["username"]))
        return redirect(request.referrer)
    
    db = udb.get_udb_flask()
    manageable = db.get_userinfo(fromUser=g.user["username"])
    return render_template("manage.html", tomanage=manageable)

@bp_auth.put("/<username>/")
@check_user
def manage_perms(username):
    db = udb.get_udb_flask()
    updatedPerms = []

    if request.form.get("all", False, bool):
        returned = db.edit_user(g.user["username"], username, ["all",])
        if isinstance(returned, str):
            flash(returned)
            return redirect(url_for("index"), 303)
        flash(f"Permissions for {username} updated.", "info")
        if not request.referrer:
            return redirect(url_for(".user_manager"), 303)
        return redirect(request.referrer, 303)
    
    if request.form.get("manage_users",False,bool):
        updatedPerms.append("manage_users")
    if request.form.get("send_downlinks",False,bool):
        updatedPerms.append("send_downlinks")
    if request.form.get("active",False,bool):
        updatedPerms.append("active")

    returned = db.edit_user(g.user["username"], username, updatedPerms)
    if isinstance(returned, str):
        flash(returned, "error")
        return redirect(url_for("index"), 303)
    flash(f"Permissions for {username} updated.", "info")
    if not request.referrer:
        return redirect(url_for(".user_manager"), 303)
    return redirect(request.referrer, 303)

@bp_auth.delete("/<username>/")
@check_user
def remove_user(username):
    if "all" not in g.user["permissions"] \
        and g.user["username"] != username:
        flash("Lacking permissions to remove other users.", "error")
        if request.referrer:
            return redirect(request.referrer, 303)
        return redirect(url_for(".profile", username=g.user["username"]), 303)

    db = udb.get_udb_flask()
    res = db.remove_user(g.user["username"], username)
    if res:
        flash(res, "error")
        return redirect(url_for(".user_manager"), 303)
    flash(f"Deleted {username} successfully.", "warning")
    if username == g.user["username"]:
        session.clear()
        flash(f"Deleted {username} successfully.", "warning")
        return redirect(url_for("index"), 303)
    return redirect(url_for(".user_manager"), 303)

@bp_auth.route("/<username>", methods=("GET", "PUT", "DELETE"))
def redirect_users(username):
    if request.method == "GET":
        return redirect(url_for(".profile", username=username), 301)
    elif request.method == "PUT":
        return redirect(url_for(".manage_perms", username=username), 301)
    else:
        return redirect(url_for(".remove_user", username=username), 301)

@bp_auth.before_app_request
def set_user():
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        db = udb.get_udb_flask()
        userinfo =  db.get_userinfo(user_id)
        if isinstance(userinfo, dict):
            g.user = userinfo
        else:
            g.user = None
