from datetime import datetime
import json
import logging, logging.handlers
import pathlib
import sqlite3
import threading
import contextlib
import shutil
from typing import Literal, Optional
import click
from time import sleep
from flask import current_app, g
from flask.cli import with_appcontext

from werkzeug.security import check_password_hash, generate_password_hash

class UserDB(object):
    """
    Class for managing user data in an sqlite db.
    """

    MQTT_DOWNLINK_SKIPPED = datetime(1977, 7, 7)

    def __init__(self, path: pathlib.Path | str, backup: Optional[pathlib.Path | str]=None, timeout: int=30, long_timeout: int=60, logqueue=None):

        if path != ":memory:":
            self._db = sqlite3.connect(database=path, timeout=timeout, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        else:
            self.logger = logging.Logger(f"{__name__}.{path}")
            raise Exception("User DB needs to be in a file")
        self._db.row_factory = sqlite3.Row

        self.dbpath = pathlib.Path(path)
        self.timeout = timeout
        self.long_timeout = long_timeout
       
        self.logger = logging.Logger(f"{__name__}.{self.dbpath.name}")
        self.logger.setLevel(logging.INFO)
        if ( not self.logger.hasHandlers() ) and logqueue:
            self.logger.addHandler( logging.handlers.QueueHandler(logqueue) )

        if backup:
            try:
                self.backup = sqlite3.Connection(database=backup, timeout=timeout)
            except: # pragma: no coverage
                self.logger.error(f"Couldn't connect to {backup} for creating a backup!", exc_info=1)
                self.logger.error("Continuing without one.")
                self.bckpath = None
            else:
                self.backup.close()
                self.bckpath = pathlib.Path(backup)
                self.backup_thread = threading.Thread(target=self._backup, daemon=True)
                self.backup_thread.do_run = True
                self.backup_thread.start()
            finally:
                del self.backup
        else:
            self.logger.debug("No backup DB specified. Continuing without one!")
            self.bckpath = None

        self.logger.debug("Initialized User DB.")
        self.logger.debug(f"Main DB: {self.dbpath}")
        if self.bckpath:
            self.logger.debug(f"Backup DB: {self.bckpath}")
        self.errorcnt = 0

        self._db.close()
        del self._db

    def __del__(self):
        # Stop backup thread
        try:
            if getattr(self, "backup_thread", None):
                self.backup_thread.do_run = False
                self.backup_thread.join()
        except Exception as e: # pragma: no coverage
            self.logger.debug(f"Exception during DB destruction: {e}")
        self.logger.debug("Database closed.")

    def transaction_setup(self):
        db = sqlite3.connect(database=self.dbpath, timeout=self.timeout, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        db.row_factory = sqlite3.Row

        return db

    def init_db(self):
        with contextlib.closing(self.transaction_setup()) as db:
            db.execute("DROP TABLE IF EXISTS user")
            db.execute("DROP TABLE IF EXISTS downlinks")
            db.execute("""
                CREATE TABLE user(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    username TEXT UNIQUE NOT NULL,
                    passwd TEXT NOT NULL,
                    permissions JSON
                )
            """) # Permissions as json list: all, manage_users, send_downlinks, active
            db.execute("""
                CREATE TABLE downlinks(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    user_id INTEGER NOT NULL,
                    device_id TEXT NOT NULL,
                    downlink TEXT NOT NULL,
                    sent TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    mqtt_sent TIMESTAMP,
                    FOREIGN KEY (user_id) REFERENCES user (id)
                )
            """) # When not in separate files: FOREIGN KEY (device_id) REFERENCES status (id)
            db.commit()
    
    def __em_backup(self, *files):
        shutil.copyfile(src=files[0], dst=files[1])

    def __emergency(self, source: str, target: str) -> bool:
        backups = threading.Thread(target=self.__em_backup, args=(source, target))
        backups.start()
        backups.join(self.long_timeout)
        #backups.terminate()

        if not backups.is_alive():
            return True
        else: # pragma: no coverage
            return False

    def __snapshot_db(self):
        dbdir = self.dbpath.parent
        dbname = "udb_error" + str(self.errorcnt) + ".db"
        self.errordb = dbdir / dbname
        
        self.logger.error(f"Creating a backup of the database at {self.errordb}...")
        if hasattr(self, 'backup_thread'): # pragma: no coverage
            self.backup_thread.do_run = False
        
        self.logger.error("Starting snapshot.")
        success = self.__emergency(source=self.dbpath, target=self.errordb)
        if success:
            self.logger.warning("Snapshot finished successfully.")
        else: # pragma: no coverage
            self.logger.error("Snapshot failed.")

        self.errorcnt += 1
        if hasattr(self, 'backup_thread'): # pragma: no coverage
            self.logger.warning("Restarting regular backup process.")
            if not self.backup_thread.is_alive():
                self.backup_thread = threading.Thread(target=self._backup)
                self.backup_thread.do_run = True
                self.backup_thread.start()
            else: # pragma: no coverage
                # Backup thread is usually very fast in recognizing it is no longer needed
                self.backup_thread.do_run = True

    def add_user(self, user: str, passwd: str) -> str|None :
        with contextlib.closing(self.transaction_setup()) as db:
            try:
                db.execute("""
                    INSERT INTO user (username, passwd, permissions) 
                    VALUES (?,?,?)
                """, (user, generate_password_hash(passwd), '{"permissions":[]}'))
                db.commit()
            except sqlite3.IntegrityError:
                return "User already registered"
            except sqlite3.OperationalError as e:
                # DB most likely unavailable due to another transaction in progress; make a copy, but don't restore
                self.logger.exception(f"Error adding user {user} to DB: {e}")
                self.__snapshot_db()
                return "Internal Server Error"
            except: # pragma: no coverage
                self.logger.critical(f"Error adding user {user} to DB", exc_info=1)
                return "Internal Server Error"
            else:
                self.logger.info("Added user %s", user)
                return None
    
    def check_user(self, user: str, passwd: str) -> int|Literal["Invalid username or password"]:
        with contextlib.closing(self.transaction_setup()) as db:
            cursor = db.execute("SELECT * FROM user WHERE username=? LIMIT 1", (user,))
            userdata = cursor.fetchone()

            if (userdata is None):
                return "Invalid username or password"
            elif not check_password_hash(userdata["passwd"], passwd):
                return "Invalid username or password"
            return userdata["id"]
    
    def get_userinfo(self, user: Optional[int|str]=None, fromUser: Optional[int|str]=None): # Specify either user to look up or user trying to get all userinfo
        if not (isinstance(user, int) or isinstance(user, str)):
            # Trying to get all users (for managing them?)
            if fromUser:
                with contextlib.closing(self.transaction_setup()) as db:
                    try:
                        if isinstance(fromUser, int):
                            info = db.execute("""
                                SELECT id, username, permissions FROM user WHERE id=? LIMIT 1
                            """, (fromUser,)).fetchone()
                            if info: # If the fetch was empty, none is returned
                                perms = json.loads(info["permissions"]).get("permissions")
                        else:
                            info = db.execute("""
                                SELECT id, username, permissions FROM user WHERE username=? LIMIT 1
                            """, (fromUser,)).fetchone()
                            if info:
                                perms = json.loads(info["permissions"]).get("permissions")
                    except json.JSONDecodeError: # pragma: no coverage
                        perms = []
                
                    if info and ("all" in perms or "manage_users" in perms):
                        allinfo = db.execute("""
                            SELECT DISTINCT username, permissions FROM user
                        """).fetchall()
                        return [dict(u) for u in allinfo]
                    else:
                        return "No permissions for looking up users"
            else:
                return "No identifier for user given"
        
        # Else, just look up specified user
        with contextlib.closing(self.transaction_setup()) as db:
            try:
                if isinstance(user, int):
                    info = db.execute("""
                        SELECT id, username, permissions FROM user WHERE id=? LIMIT 1
                    """, (user,)).fetchone()
                    if info: # If the fetch was empty, none is returned
                        perms = json.loads(info["permissions"]).get("permissions")
                else:
                    info = db.execute("""
                        SELECT id, username, permissions FROM user WHERE username=? LIMIT 1
                    """, (user,)).fetchone()
                    if info:
                        perms = json.loads(info["permissions"]).get("permissions")
            except json.JSONDecodeError: # pragma: no coverage
                perms = []
            
            if not info: # Non-existent user
                return {"permissions": [], "id": None, "user": None}
            return {
                "id": info["id"],
                "username": info["username"],
                "permissions": perms
            }
            
    def edit_user(self, editor: str, edited: str, newperms: list[str]):
        perms_parsed = self.get_userinfo(editor)
        if not perms_parsed.get("id"):
            return "Invalid permissions"
        perms_parsed = perms_parsed["permissions"]

        # If moderator (only manage_users): Can only allow users to join/send downlinks
        # If admin, can allow new moderators and admins 
        if ("manage_users" not in perms_parsed) and ("all" not in perms_parsed):
            return "Invalid permissions"
        if ("manage_users" in newperms or "all" in newperms) and ("all" not in perms_parsed):
            return "Invalid permissions"
        
           
        edited_perms = self.get_userinfo(edited)
        if not edited_perms.get("id"):
            return "User does not exist"
        edited_perms = edited_perms.get("permissions")

        if ("all" in edited_perms): # Cannot edit admins 
            return "Cannot edit Admins"

        with contextlib.closing(self.transaction_setup()) as db:
            newperms_string = json.dumps({"permissions": newperms})
            try:
                db.execute("""
                    UPDATE user
                    SET permissions=?
                    WHERE username=?
                """, (newperms_string, edited))
                db.commit()
                self.logger.info(f"User {editor} edited permissions of user {edited}:")
                self.logger.info(f"{json.dumps(newperms)}")
            except sqlite3.OperationalError as e: # pragma: no coverage
                # Snapshot is already tested for add_user
                # DB most likely unavailable due to another transaction in progress; make a copy, but don't restore
                self.logger.exception(f"Error editing user {edited} in DB: {e}")
                self.logger.error("Editor: %s", editor)
                self.logger.error("Permissions: %s", newperms_string)
                self.__snapshot_db()
                return "DB Error while updating permissions"
            except: # pragma: no coverage
                self.logger.critical(f"Error editing user {edited} in DB", exc_info=1)
                self.logger.error("Editor: %s", editor)
                self.logger.error("Permissions: %s", newperms_string)
                return "DB Error while updating permissions"
            return None
    
    def remove_user(self, admin: str, snapped_user: str):
        # Only usable by admins.
        perms = self.get_userinfo(admin)["permissions"]
        if not "all" in perms and admin != snapped_user:
            return "Lacking permissions to remove users."

        with contextlib.closing(self.transaction_setup()) as db:
            try:
                lastinfo = self.get_userinfo(snapped_user)
                if not lastinfo.get("id"):
                    raise sqlite3.DataError
                if "all" in lastinfo.get("permissions"):
                    self.logger.warning(f"Admin {admin} tried to delete Admin {snapped_user}!")
                    return "Can't delete other Admins. Contact sysadmin"
                db.execute("""DELETE FROM user
                    WHERE username=?
                """, (snapped_user,))
                db.commit()
            except sqlite3.DataError:
                return "Targeted user does not exist"
            except sqlite3.OperationalError as e: # pragma: no coverage
                # Snapshot is already tested for add_user
                # DB most likely unavailable due to another transaction in progress; make a copy, but don't restore
                self.logger.exception(f"Error removing user {snapped_user} in DB: {e}")
                self.logger.error("Attempt made by: %s", admin)
                self.__snapshot_db()
                return "DB Error"
            except: # pragma: no coverage
                self.logger.critical(f"Error removing user {snapped_user} in DB", exc_info=1)
                self.logger.error("Attempt made by: %s", admin)
                return "DB Error"
            self.logger.warning(f"User {admin} deleted user {snapped_user}.")
            return None
    
    def add_downlink(self, user: str, device: str, downlink: dict) -> str|None:
        cleaned_downlink = {}
        for key in ("CTIME", "STIME", "NAME"):
            cleaned_downlink.update({key: downlink.get(key)})
            if not cleaned_downlink.get(key):
                cleaned_downlink.pop(key, None)

        # Ensure CTIME and STIME are integers, NAME is max 8 chars long
        for key in ("CTIME", "STIME"):
            try:
                oldval = cleaned_downlink.pop(key)
                cleaned_downlink.update({key: int(oldval)})
            except:
                self.logger.error(f"Invalid {key} value in downlink from {user}: {cleaned_downlink.get(key)}")
        if cleaned_downlink.get("NAME") and len(cleaned_downlink.get("NAME")) > 8:
            badval = cleaned_downlink.pop("NAME")
            self.logger.error(f"Invalid NAME value in downlink from {user}: {badval}")
        
        if not cleaned_downlink:
            return "Invalid downlink"
        try:
            info = self.get_userinfo(user)
            downlink_bundle = {
                'user_id': info["id"],
                'perms': info["permissions"]
            }
        except: # pragma: no coverage
            self.logger.error(f"Failed to parse permissions for {user}'s downlink")
            return "Error parsing permissions"

        downlink_bundle.update({
            "downlink": json.dumps(downlink),
            "device": device
        })

        # Since the status table is in another file, can't add a constraint for the device id to avoid sending to unknown devices
        if ("send_downlinks" not in downlink_bundle["perms"]) and ("all" not in downlink_bundle["perms"]):
            return "Invalid permissions"
        else:
            with contextlib.closing(self.transaction_setup()) as db:
                try:
                    downlink_bundle.update({"timestamp": datetime.now()})
                    db.execute("""
                        INSERT INTO downlinks (user_id, device_id, downlink, sent) VALUES (
                            :user_id,:device,:downlink,:timestamp
                        ) 
                    """, downlink_bundle )
                    id = db.execute("""SELECT id FROM downlinks 
                        WHERE user_id=:user_id 
                        AND device_id=:device 
                        AND downlink=:downlink 
                        AND sent=:timestamp
                        LIMIT 1""", downlink_bundle)
                    db.commit()
                    # send downlink id to mqtt task
                except sqlite3.OperationalError: # pragma: no coverage
                    self.logger.exception(f"Error saving downlink by {user} in DB")
                    self.logger.error("For device: %s, content: %s", device, downlink)
                    self.__snapshot_db()
                    return "Error saving downlink"
                except: # pragma: no coverage
                    self.logger.critical(f"Error saving downlink by {user} in DB", exc_info=1)
                    self.logger.error("For device: %s, content: %s", device, downlink)
                    return "DB Error"
                return None

    def get_downlink(self, id: int) -> dict|None:
        with contextlib.closing(self.transaction_setup()) as db:
            try:
                downlink = db.execute("""
                SELECT device_id, downlink FROM downlinks
                WHERE id=?
                LIMIT 1
                """, (id,)).fetchone()
            except Exception as e:
                self.logger.error(f"Failed to get downlink {id}: {e}")
                return None
            else:
                return dict(downlink)

    def update_downlink_mqttsent(self, id: int, mqtt_time: datetime):
        with contextlib.closing(self.transaction_setup()) as db:
            try:
                db.execute("""
                UPDATE downlinks
                SET mqtt_sent=?
                WHERE id=?
                """, (mqtt_time, id))
                db.commit()
                self.logger.debug(f"Sent downlink {id} via MQTT at {mqtt_time}.")
            except Exception as e:
                self.logger.error(f"Failed to update downlink time for entry {id}: {e}")
                return "DB Error"
            else:
                return None
    
    def _bck_progress(self, status, remaining, total):
        self.logger.debug(f'Copied {total-remaining} of {total} pages...')
    
    def _backup(self):
        # TODO: Rotate backups (to backup folder in instance), maybe using rotate-backups
        t = threading.current_thread()
        while getattr(t, "do_run", False):
            backuper_db = sqlite3.connect(self.bckpath, timeout=20)
            backuped_db = sqlite3.connect(self.dbpath, timeout=20)
            self.logger.info("Starting backup.")
            backuped_db.backup(backuper_db, pages=5, progress=self._bck_progress, sleep=0.5)
            self.logger.info("Backup finished.")
            backuper_db.close()
            backuped_db.close()
            i = 0
            while i < 3600 and getattr(t, "do_run", False):
                sleep(1)
                i+=1

@click.command("init-user-db")
@with_appcontext
def init_udb():
    """
    Set up a User sqlite DB in the instance folder.
    """
    db = UserDB(current_app.config["DB_USER"])
    db.init_db()
    click.echo("User DB initialized.")
    del db

@click.command("set-admin")
@click.argument("username")
@with_appcontext
def set_admin(username):
    """
    Make <username> into an admin. Can only be reset via "unset-admin <username>"
    """
    db = sqlite3.Connection(current_app.config["DB_USER"])
    try:
        if not db.execute("SELECT id FROM user WHERE username=?", (username,)).fetchone():
            raise sqlite3.DataError
        db.execute("""UPDATE user
            SET PERMISSIONS='{"permissions":["all"]}'
            WHERE username=?""", (username,))
        db.commit()
    except sqlite3.DataError:
        click.echo(f"User {username} not found in DB!", err=True)
    else:
        click.echo(f"Made user {username} an admin.")
    finally:
        db.close()

@click.command("unset-admin")
@click.argument("username")
@with_appcontext
def unset_admin(username):
    """
    Reset <username> to have no permissions, even if an admin.
    """
    db = sqlite3.Connection(current_app.config["DB_USER"])
    try:
        if not db.execute("SELECT id FROM user WHERE username=?", (username,)).fetchone():
            raise sqlite3.DataError
        db.execute("""UPDATE user
            SET PERMISSIONS='{"permissions":[]}'
            WHERE username=?""", (username,))
        db.commit()
    except sqlite3.DataError:
        click.echo(f"User {username} not found in DB!", err=True)
    else:
        click.echo(f"Reset user {username}.")
    finally:
        db.close()

def get_udb_flask() -> UserDB:
    if 'udb' not in g: # pragma: no branch
        g.udb = UserDB(current_app.config["DB_USER"], logqueue=current_app.logqueue)
    
    return g.udb

def teardown_udb(e=None):
    db = g.pop("udb", None)

    if db is not None:
        del db

def init_udb_flask(app):
    app.teardown_appcontext(teardown_udb)
    app.cli.add_command(init_udb)
    app.cli.add_command(set_admin)
    app.cli.add_command(unset_admin)
