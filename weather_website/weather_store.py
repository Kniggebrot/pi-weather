#!/usr/bin/env python3
"""
Functions for storing and getting measurements from a sqlite database, via the MeasureDB class.
"""

import pathlib
import time
import shutil
import click
from flask import Flask, current_app, g
from flask.cli import with_appcontext

from typing import Optional
from time import sleep
import sqlite3, threading, os
import logging, logging.handlers
import functools
import json

class MeasureDB(object):
    """Class for DB with measurements and device status"""
    def __init__(self, database: Optional[pathlib.Path | str]=None, backup: Optional[pathlib.Path | str]=None, timeout: int=30, long_timeout: int=60, logqueue=None):
        self.timeout = timeout
        self.long_timeout = long_timeout
        if database is None:
            self._database = sqlite3.connect(":memory:", detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES, timeout=self.timeout)
            self.dbpath = ":memory:"
        else:
            self._database = sqlite3.connect(database=database, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES, timeout=self.timeout)
            self.dbpath = pathlib.Path(database)
        self.errordb = pathlib.Path("")
 
        self.logger = logging.getLogger(f"{__name__}.{getattr(self.dbpath, 'name', ':memory:')}")
        self.logger.setLevel(logging.INFO)
        if ( not self.logger.hasHandlers() ) and logqueue:
            self.logger.addHandler( logging.handlers.QueueHandler(logqueue) )

        # Setup example cursor, then close it and connection if not using memory db
        self._database.row_factory = sqlite3.Row
        self._cursor = self._database.cursor()
        if self.dbpath != ":memory:":
            self._cursor.close()
            self._database.close()
        
        if backup:
            try:
                self.backup_db = sqlite3.connect(backup)
            except Exception as e: # pragma: no coverage
                self.logger.error(f"Error opening database {backup}: {e}. Continuing without backup!")
                self.backup_db = None
            else:
                self.backup_db.close()
                self.backuppath = backup
                self.backup_thread = threading.Thread(target=self._backup, daemon=True)
                self.backup_thread.do_run = True
                self.backup_thread.start()
            finally:
                del self.backup_db

        self.logger.debug(f"Using DB {self.dbpath}")
        if getattr(self, 'backuppath', None):
            self.logger.debug(f"with backup {self.backuppath}.")
        else:
            self.logger.debug("with NO backup.")
        self.errorcnt = 0

    def __del__(self):
        self.logger.debug("DB destruction started.")
        try: # No database connection needs to be stopped, but still...
            if hasattr(self, 'backup_thread'):
                self.backup_thread.do_run = False
            self._database.commit()
            self._database.close()
        except Exception as e:
            self.logger.debug(f"Exception during DB destruction: {e}")
        # Backup_db is not used after init anymore, so it is either closed or None
        self.logger.debug("Database stopped.")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            if self._database.in_transaction:
                self._database.commit()
        except:
            pass
        finally:
            self._database.close()

    def Connection_Decorator(function): # Decorator to open and close database connection per transaction
    
        @functools.wraps(function)
        def transaction(self, *args, **kwargs):
            if self.dbpath != ":memory:":
                self._database = sqlite3.connect(self.dbpath, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES, timeout=self.timeout)
                self._database.row_factory = sqlite3.Row
                self._cursor = self._database.cursor()

            result = function(self, *args, **kwargs)

            if self.dbpath != ":memory:":
                self._cursor.close()
                self._database.close()

            return result
            # End of inner function

        return transaction

    @Connection_Decorator
    def init_tables(self):
        self._cursor.execute("""DROP TABLE IF EXISTS measurements""")
        self._cursor.execute("""DROP TABLE IF EXISTS status""")
        self._cursor.execute("""DROP TABLE IF EXISTS config""")
        self._cursor.execute("""CREATE TABLE measurements(
            device_id string,
            time int,
            temperature real,
            pressure real,
            humidity real
        ) """) # Good to add: FOREIGN KEY (device_id) REFERENCES status (device_id), more not null...
        self._cursor.execute("""CREATE TABLE status(
            device_id string,
            name string,
            lastreset string,
            time int,
            chiptemp real,
            voltage int,
            rssi int
        ) """)
        self._cursor.execute("""CREATE TABLE config(
            device_id string PRIMARY KEY,
            seen_at int,
            cfg_json string
        ) """)
        self._database.commit()
    
    def _em_backup(self, **files):
        shutil.copyfile(src=files["source"], dst=files["target"])

    def _emergency(self, source: str, target: str) -> bool:
        backups = threading.Thread(target=self._em_backup, kwargs={"source": source, "target": target})
        backups.start()
        backups.join(self.long_timeout)
        #backups.terminate()

        if not backups.is_alive():
            return True
        else: # pragma: no coverage
            return False

    def _restore_db(self, exception):
        if self.dbpath != ":memory:":
            dbdir = self.dbpath.parent
            dbname = "error" + str(self.errorcnt) + ".db"
            self.errordb = dbdir / dbname
        else: # pragma: no coverage
            # test takes too long, so no repetition for in-memory dbs
            self.errordb = "error" + str(self.errorcnt) + ".db"
        self.logger.critical(f"An exception happened during an SQL operation: {exception}")
        self.logger.critical(f"Creating a backup of the database at {self.errordb}, then re-initializing the db...")
        if hasattr(self, 'backup_thread'): # pragma: no coverage
            self.backup_thread.do_run = False
        
        self.logger.error("Starting backup.")
        success = self._emergency(source=self.dbpath, target=self.errordb)
        if success:
            self.logger.warning("Backup finished successfully.")
        else: # pragma: no coverage
            self.logger.error("Backup failed.")

        self.errorcnt += 1
        old_timeout = self.timeout
        self.timeout = self.long_timeout
        self.init_tables()
        self.logger.critical(f"Re-initialization complete.")
        self.timeout = old_timeout
        if hasattr(self, 'backup_thread'): # pragma: no coverage
            self.logger.warning("Restarting regular backup process.")
            if not self.backup_thread.is_alive():
                self.backup_thread = threading.Thread(target=self._backup)
                self.backup_thread.do_run = True
                self.backup_thread.start()
            else: # pragma: no coverage
                # Backup thread is usually very fast in recognizing it is no longer needed
                self.backup_thread.do_run = True
        # Because of the connection decorator, a db connection may need to be reopened
        if self.dbpath != ":memory:": # pragma: no coverage
            self._database = sqlite3.connect(self.dbpath, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES, timeout=self.timeout)
            self._database.row_factory = sqlite3.Row
            self._cursor = self._database.cursor()

    @Connection_Decorator
    def add_measurement(self, measurement: dict):
        try:
            self._cursor.execute("""INSERT INTO measurements VALUES(
                :id, :timestamp, :temp, :pres, :humi
                ) """, measurement)
            self._database.commit()
        except sqlite3.OperationalError as e:
            self._restore_db(e)

    @Connection_Decorator
    def add_status(self, status: dict):
        try:
            self._cursor.execute("""INSERT INTO status VALUES(
                :id, :name, :lastreset, :timestamp, :chiptemp, :voltage, :rssi
                ) """, status)
            self._database.commit()
        except sqlite3.OperationalError as e: # pragma: no coverage
            # Already tested in add_measurement, and takes relatively long
            self._restore_db(e)

    @Connection_Decorator
    def add_config(self, config: dict):
        device_id = config.get("id")
        if device_id is None:
            self.logger.error("failed to add config: no device_id specified!")
            self.logger.error(f"Cfg: {config}")
            return
        try:
            # Check if cfg entries for device exist; could also use COUNT()
            self._cursor.execute("""
            SELECT cfg_json FROM config
            WHERE device_id=?
            """, (device_id,))
            if self._cursor.fetchone() is None:
                self._cursor.execute("""
                INSERT INTO config VALUES(
                :id, :seen_at, :cfg_json
                ) """, config)
            else:
                self._cursor.execute("""UPDATE config
                SET seen_at=:seen_at, cfg_json=:cfg_json
                WHERE device_id=:id
                """, config)
            self._database.commit()
        except sqlite3.OperationalError as e: # pragma: no coverage
            # Already tested in add_measurement, and takes relatively long
            self._restore_db(e)


    @Connection_Decorator
    def get_devices(self)-> (list[dict] | int):
        try:
            devicedict = []
            self._cursor.execute("SELECT DISTINCT device_id FROM status")    # All devices will send at least one status when successfully connected
            devices = self._cursor.fetchall()
            for uid in devices:
                self._cursor.execute("""SELECT name FROM status 
                WHERE device_id=?
                AND name NOTNULL
                ORDER BY time DESC
                LIMIT 1
                """, uid)
                res = self._cursor.fetchone()
                if res: # Name is not empty
                    devicedict.append(dict(id=uid[0], name=res[0]))
                else:
                    devicedict.append(dict(id=uid[0], name=""))
            return devicedict
        except sqlite3.OperationalError as e:
            return 500
            #self._restore_db(e)

    @Connection_Decorator
    def get_dashboard(self, devices: list[dict]) -> list[dict]:
        dashboard_list = []
        for device in devices:
            status = self._cursor.execute("""SELECT time, chiptemp, voltage, rssi FROM status 
                WHERE device_id=?
                ORDER BY time DESC
                LIMIT 1
                """, (device["id"],)).fetchone()

            try:
                measure = self._cursor.execute("""
                    SELECT time, temperature, pressure, humidity
                    FROM measurements
                    WHERE device_id=?
                    ORDER BY time DESC
                    LIMIT 1
                """, (device["id"],)).fetchone()
            except:
                measure = None
            
            device_summary = {
                "id": device["id"],
                "name": device["name"],
                
                # Status options have to exist
                "status": dict(status)
            }
            if measure:
                device_summary.update({"measure": dict(measure)})

            dashboard_list.append(device_summary)

        return dashboard_list

    @Connection_Decorator
    def get_measurements(self, device: str, count=1) -> list[dict] | int:
        if device:
            try:
                self._cursor.execute("""SELECT time, temperature, pressure, humidity FROM measurements 
                WHERE device_id=?
                ORDER BY time DESC
                LIMIT ?
                """, (device, count))
                result = self._cursor.fetchmany(count)
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400
            #raise sqlite3.NotSupportedError("No device ID specified!")

    @Connection_Decorator
    def get_measurements_time(self, device: str, time_lower=int(time.time())-86400, time_higher=int(time.time())) -> (list[dict] | int):
        """
        Get measurements in a certain timeframe; default timeframe: yesterday - now
        """
        if device:
            try:
                self._cursor.execute("""SELECT time, temperature, pressure, humidity FROM measurements 
                WHERE device_id=?
                AND time BETWEEN ? AND ?
                ORDER BY time ASC
                """, (device, time_lower, time_higher))
                result = self._cursor.fetchall()
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400
            #raise sqlite3.NotSupportedEr

    @Connection_Decorator
    def get_status(self, device: str, count=1) -> (list[dict] | int):
        if device:
            try:
                self._cursor.execute("""SELECT name, lastreset, time, chiptemp, voltage, rssi FROM status 
                WHERE device_id=?
                ORDER BY time DESC
                LIMIT ?
                """, (device,count))
                result = self._cursor.fetchmany(count)
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400
            #raise sqlite3.NotSupportedError("No device ID specified!")

    @Connection_Decorator
    def get_status_time(self, device: str, time_lower=int(time.time())-86400, time_higher=int(time.time())) -> (list[dict] | int):
        if device:
            try:
                self._cursor.execute("""SELECT name, lastreset, time, chiptemp, voltage, rssi FROM status 
                WHERE device_id=?
                AND time BETWEEN ? AND ?
                ORDER BY time ASC
                """, (device, time_lower, time_higher))
                result = self._cursor.fetchall()
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400

    @Connection_Decorator
    def get_fullstatus(self, device: str, count=1) -> (list[dict] | int):
        if device:
            try:
                self._cursor.execute("""SELECT name, lastreset, time, chiptemp, voltage, rssi FROM status 
                WHERE device_id=?
                AND name NOTNULL
                ORDER BY time DESC
                LIMIT ?
                """, (device,count))
                result = self._cursor.fetchmany(count)
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400
            # raise sqlite3.NotSupportedError("No device ID specified!")

    @Connection_Decorator
    def get_fullstatus_time(self, device: str, time_lower=int(time.time())-86400, time_higher=int(time.time())) -> (list[dict] | int):
        if device:
            try:
                self._cursor.execute("""SELECT name, lastreset, time, chiptemp, voltage, rssi FROM status 
                WHERE device_id=?
                AND name NOTNULL
                AND time BETWEEN ? AND ?
                ORDER BY time ASC
                """, (device, time_lower, time_higher))
                result = self._cursor.fetchall()
                return [dict(r) for r in result]
            except sqlite3.OperationalError as e:
                return 500
                #self._restore_db(e)
        else:
            return 400
        
    @Connection_Decorator
    def get_config(self, device: str) -> dict | int:
        if device:
            try:
                self._cursor.execute("""SELECT seen_at, cfg_json FROM config
                WHERE device_id=?
                LIMIT 1
                """, (device,))
                res: sqlite3.Row = self._cursor.fetchone()
                if res:
                    result = {
                        "device_id": device,
                        "seen_at": res["seen_at"],
                        "cfg": json.loads(res["cfg_json"]),
                    }
                    return result
                return {}
            except sqlite3.OperationalError as e:
                return 500
        else:
            return 400

    def _bck_progress(self, status, remaining, total):
        self.logger.debug(f'Copied {total-remaining} of {total} pages...')
    
    def _backup(self):
        # TODO: Rotate backups (to backup folder in instance), maybe using rotate-backups
        t = threading.current_thread()
        while getattr(t, "do_run", False):
            backuper_db = sqlite3.connect(self.backuppath, timeout=20)
            backuped_db = sqlite3.connect(self.dbpath, timeout=20)
            self.logger.info("Starting backup.")
            backuped_db.backup(backuper_db, pages=5, progress=self._bck_progress, sleep=0.5)
            self.logger.info("Backup finished.")
            backuper_db.close()
            backuped_db.close()
            i = 0
            while i < 3600 and getattr(t, "do_run", False):
                sleep(1)
                i+=1

def get_db(dbpath: Optional[str]=None, backupdb: Optional[str]=None, logqueue=None) -> MeasureDB:
    needs_init = not(os.path.isfile(dbpath))        # If file is not existent yet, the tables need to be initialised; assumes the tables have been initialized if trying to connect to an existing database
    db = MeasureDB(dbpath, backup=backupdb, logqueue=logqueue)
    if needs_init:
        db.init_tables()
    return db

def get_db_flask() -> MeasureDB:
    if 'db' not in g: # pragma: no coverage
        if not current_app.config["TESTING"]:
            g.db = MeasureDB(
                database=current_app.config['DB_WEATHER'],
                logqueue=current_app.logqueue,
            )
        else: 
            g.db = MeasureDB(
                database=current_app.config['DB_WEATHER'],
                timeout=1,
                long_timeout=5,
                logqueue=current_app.logqueue,
            )

    return g.db

def close_db_flask(e=None):
    db = g.pop('db', None)

    if db is not None:
        del db

def init_flask(app: Flask):
    app.teardown_appcontext(close_db_flask)
    app.cli.add_command(init_db_cli)

def init_db():
    db = get_db_flask()
    db.init_tables()

@click.command('init-weather-db')
@with_appcontext
def init_db_cli():
    """
    Initialize Weather sqlite DB with standard columns etc.
    """
    init_db()
    click.echo("Weather DB initialized.")

if __name__ == '__main__': # pragma: no cover
    pass