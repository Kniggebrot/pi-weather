import atexit
import logging, logging.handlers
from multiprocessing import JoinableQueue
import os
import pathlib
import signal
import sys
import threading
from flask import current_app

class CustomQueueListener(logging.handlers.QueueListener):
    """
    See https://stackoverflow.com/questions/25585518/python-logging-logutils-with-queuehandler-and-queuelistener (Hos)

    To be able to dynamically add handlers to a QueueListener, the base class has to be extended.
    """
    def __init__(self, queue, *handlers, respect_handler_level: bool = ...) -> None:
        super().__init__(queue, *handlers, respect_handler_level=respect_handler_level)

        self.handlers = list(self.handlers) # to allow dynamic adding/removing of handlers

    def addHandler(self, hdlr):
        """
        Add the specified handler to this logger.
        """
        if not (hdlr in self.handlers):
            self.handlers.append(hdlr)

    def removeHandler(self, hdlr: logging.Handler):
        """
        Remove the specified handler from this logger.
        """
        if hdlr in self.handlers:
            hdlr.close()
            self.handlers.remove(hdlr)

def setup_logging():
    logqueue = JoinableQueue(-1)
    # Time for better logging
    # StreamHandler for Console output, to stdout, from DEBUG lvl and up
    console_debug = logging.StreamHandler(sys.stdout)
    console_debug.setLevel(logging.DEBUG)
    console_debug.addFilter(lambda record: record.levelno == logging.DEBUG)
    console_info = logging.StreamHandler(sys.stdout)
    console_info.setLevel(logging.INFO)
    console_info.addFilter(lambda record: record.levelno == logging.INFO)
    # StreamHandler to stderr for WARNING lvl and up
    console_warn = logging.StreamHandler(sys.stderr)
    console_warn.setLevel(logging.WARNING)
    console_warn.addFilter(lambda record: record.levelno == logging.WARNING)
    console_error = logging.StreamHandler(sys.stderr)
    console_error.setLevel(logging.ERROR)
    console_error.addFilter(lambda record: record.levelno == logging.ERROR)
    console_crit = logging.StreamHandler(sys.stderr)
    console_crit.setLevel(logging.CRITICAL)
    console_crit.addFilter(lambda record: record.levelno == logging.CRITICAL)
    # Formatters per level; color coded lvl
    fmt_debug = logging.Formatter("\x1b[37m%(levelname)s\x1b[0m\t%(name)s: %(message)s")
    fmt_info  = logging.Formatter("\x1b[36m%(levelname).4s\x1b[0m\t%(name)s: %(message)s")
    fmt_warn  = logging.Formatter("\x1b[33m%(levelname).4s\x1b[0m\t%(name)s: %(message)s")
    fmt_err   = logging.Formatter("\x1b[31m%(levelname).3s\x1b[0m\t%(name)s: %(message)s")
    fmt_crit  = logging.Formatter("\x1b[4;31m%(levelname).4s\x1b[0m\t%(name)s: %(message)s")

    console_debug.setFormatter(fmt_debug)
    console_info.setFormatter(fmt_info)
    console_warn.setFormatter(fmt_warn)
    console_error.setFormatter(fmt_err)
    console_crit.setFormatter(fmt_crit)

    listener = CustomQueueListener(logqueue, console_debug, console_info, console_warn, console_error, console_crit)
    listener.start()
    loggers = (logging.getLogger(__package__), logging.getLogger("waitress"), logging.getLogger("werkzeug")) 
    for logger in loggers:
        logger.addHandler( logging.handlers.QueueHandler(logqueue) )

    def stop_listening(signum=None, stack=None):
        listener.stop()
        logqueue.close()

    atexit.register(stop_listening)
    if threading.current_thread() is threading.main_thread():
        signal.signal(signal.SIGINT, stop_listening)
        signal.signal(signal.SIGTERM, stop_listening)
    
    return (listener, logqueue)

def setup_file_logging(app):
    with app.app_context():
        # RotatingFileHandler to log dir with some files; INFO lvl and up
        if not (current_app.testing or (os.getenv("FLASK_ENV") == "development" or os.getenv("FLASK_ENV") == "testing")): # pragma: no coverage
            pathlib.Path(app.instance_path, "logs").mkdir(exist_ok=True)
            file_handler = logging.handlers.TimedRotatingFileHandler( pathlib.Path(app.instance_path, "logs", "app_log.log"), encoding="utf-8", when='midnight', interval=1, backupCount=7)
            # Formatter with time, used for FileHandler later
            fmt_long = logging.Formatter("%(asctime)s %(levelname)s\t%(name)s: %(message)s", datefmt="%a %Y-%m-%d %H:%M:%S")
            file_handler.setFormatter(fmt_long)
            file_handler.setLevel(logging.DEBUG)
            return file_handler
        return logging.NullHandler()

def setup_file_logging_backend(logdir: str|pathlib.Path):
    # Backend is in different python processes, so it needs its own logging setup
    if not (os.getenv("FLASK_ENV") == "development" or os.getenv("FLASK_ENV") == "testing"):
        logpath = pathlib.Path(logdir)
        logpath.mkdir(exist_ok=True)
        file_handler = logging.handlers.TimedRotatingFileHandler( logpath / "backend.log", encoding="utf-8", when='midnight', interval=1, backupCount=7)
        # Formatter with time, used for FileHandler later
        fmt_long = logging.Formatter("%(asctime)s %(levelname)s\t%(name)s: %(message)s", datefmt="%a %Y-%m-%d %H:%M:%S")
        file_handler.setFormatter(fmt_long)
        file_handler.setLevel(logging.DEBUG)
        return file_handler
    return logging.NullHandler()
