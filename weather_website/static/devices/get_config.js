/* Script for finding the getting the last known cfg of a device,
 * and filling a table with it, or a div with an error message if not.
 */

function fillConfig() {
    const apiURL = window.location.pathname.replace("/devices/", "/api/devices/") + "/config"

    fetch(apiURL, { headers: {"Accept": "application/json"} })
        .then( res => res.json() )
        .then( parsed => {
            console.log(parsed)
            const cfg = parsed.config.cfg
            const sentAt = new Date(parsed.config.seen_at * 1000)

            const cfgTable = document.createElement("table")
            cfgTable.className = "table table-bordered table-hover table-responsive"

            const cfgBody = document.createElement("tbody")
            cfgTable.append(cfgBody)

            Object.keys(cfg).forEach( (setting) => {
                const cfgRow = document.createElement("tr")
                const cfgName = document.createElement("td")
                cfgName.setAttribute("scope", "row")
                cfgName.textContent = setting

                const cfgVal = document.createElement("td")
                cfgVal.textContent = cfg[setting]

                cfgRow.append(cfgName, cfgVal)
                cfgBody.append(cfgRow)
            });

            const cfgMsg = document.createElement("div")
            cfgMsg.textContent = `Received at ${sentAt.toLocaleString()}`

            document.getElementById("dev-cfg").append(cfgTable, cfgMsg)
        })
        .catch( err => {
            console.log("Error on config fetch: ", err)
            const cfgMsg = document.createElement("div")
            cfgMsg.className = "alert alert-danger"
            cfgMsg.setAttribute("role", "alert")

            cfgMsg.textContent = "Failed to fetch device config."
            document.getElementById("dev-cfg").append(cfgMsg)
        })

}

fillConfig()
