/* Script for setting up the Graphs on the detailed device page.
 * Should only be called from these. (/devices/<id>)
 */

async function mesGraphs() {
    const mes1 = document.getElementById('measure_chart1')
    const mes2 = document.getElementById('measure_chart2')

    const apiURL = window.location.pathname.replace("/devices/", "/api/devices/") + "/measure"
    const measures = await fetch(apiURL, { headers: {'Accept': 'application/json'} } )
        .then( (response) => response.json())
        .catch( (error) => {
            console.error("Error during fetching of " + apiURL + ":\n" + error)
            return {"error": error.toString()}
        })

    if (measures.error) {
        // Display error message if available
        const error_msg = document.createElement("div")
        error_msg.className = "alert alert-danger alert-dismissible fade show"
        error_msg.setAttribute("role","alert")
        
        error_msg.textContent = "Error: " + measures.error
        error_msg.insertAdjacentHTML("beforeend",'<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>')

        document.getElementById("measure_errors").insertAdjacentElement("beforeend", error_msg)
    }

    const measurements = ( measures.data ? measures.data : [] )

    const labels = measurements.map( (measure) => new Date(measure.time * 1000).toLocaleString() )
    const temps = measurements.map( (measure) => measure.temperature )
    const humi = measurements.map( (measure) => measure.humidity )
    const atmo = measurements.map( (measure) => measure.pressure )

    const mesChart1 = new Chart(mes1, {
        data: {
            datasets: [{
                type: 'line',
                label: 'Temperature [ °C ]',
                yAxisID: 'y_temp',
                data: temps,
                borderColor: 'rgb(204, 0, 0)',
                borderWidth: '2',
                backgroundColor: 'rgb(204, 0, 0)',
            }, {
                type: 'line',
                label: 'Humidity [ % rH ]',
                yAxisID: 'y_humi',
                data: humi,
                borderColor: 'rgb(0, 0, 204)',
                backgroundColor: 'rgba(0, 0, 102, 0.5)',
                fill: true,
                borderWidth: 1
            }],
            labels: labels
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            animations: false,
            datasets: {
                line: {
                    pointRadius: 0 // disable for all `'line'` datasets
                }
            },
            interaction: {
                mode: 'nearest',
                intersect: false,
            },
            scales: {
                x: {
                    ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        autoSkipPadding: 30,
                    }
                },
                y_temp: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                },
                y_humi: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    beginsAtZero: true,
                    min: 0,
                    max: 100,
                    }
                }
            }
        }
    )

    const mesChart2 = new Chart(mes2, {
        data: {
            datasets: [{
                type: 'line',
                label: 'Atmospheric pressure [ hPa ]',
                data: atmo,
                borderColor: 'rgba(0, 204, 204, 0.4)',
                borderWidth: '2',
                backgroundColor: 'rgba(0, 204, 204, 0.4)',
            },],
            labels: labels
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            animations: false,
            datasets: {
                line: {
                    pointRadius: 0 // disable for all `'line'` datasets
                }
            },
            plugins: {
                decimation: {
                    enabled: true,
                }
            },
            interaction: {
                mode: 'nearest',
                intersect: false,
            },
            scales: {
                x: {
                    ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        autoSkipPadding: 30,
                    }
                },
            }
        }
    })
}

mesGraphs()
