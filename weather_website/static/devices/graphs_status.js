/* Script for setting up the Graphs on the detailed device page.
 * Should only be called from these. (/devices/<id>)
 */

async function statGraphs() {
    const stat1 = document.getElementById('status_chart1')
    const stat2 = document.getElementById('status_chart2')

    const apiURL = window.location.pathname.replace("/devices/", "/api/devices/") + "/status"
    const statses = await fetch(apiURL, { headers: {'Accept': 'application/json'} } )
        .then( (response) => response.json())
        .catch( (error) => {
            console.error("Error during fetching of " + apiURL + ":\n" + error)
            return {"error": error.toString()}
        })

    if (statses.error) {
        // Display error message if available
        const error_msg = document.createElement("div")
        error_msg.className = "alert alert-danger alert-dismissible fade show"
        error_msg.setAttribute("role","alert")
        
        error_msg.textContent = "Error: " + statses.error
        error_msg.insertAdjacentHTML("beforeend",'<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>')

        document.getElementById("status_errors").insertAdjacentElement("beforeend", error_msg)
    }

    const statuses = ( statses.data ? statses.data : [] )

    const labels_stat = statuses.map( (statentry) => new Date(statentry.time * 1000).toLocaleString() )
    const temp_stat = statuses.map( (statentry) => statentry.chiptemp )
    const vcc = statuses.map( (statentry) => statentry.voltage )
    const rssi = statuses.map( (statentry) => statentry.rssi )

    const statChart1 = new Chart(stat1, {
        data: {
            datasets: [{
                type: 'line',
                label: 'Chip temperature [ °C ]',
                yAxisID: 'y_temp',
                data: temp_stat,
                borderColor: 'rgb(204, 0, 0)',
                borderWidth: '1',
                backgroundColor: 'rgb(204, 0, 0)',
            }, {
                type: 'line',
                label: 'Supply voltage [ mV ]',
                yAxisID: 'y_vcc',
                data: vcc,
                borderColor: 'rgb(204, 204, 0)',
                backgroundColor: 'rgba(204, 204, 0, 0.5)',
            }],
            labels: labels_stat
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            animations: false,
            datasets: {
                line: {
                    pointRadius: 0 // disable for all `'line'` datasets
                }
            },
            plugins: {
                decimation: {
                    enabled: true,
                }
            },
            interaction: {
                mode: 'nearest',
                intersect: false,
            },
            scales: {
                x: {
                    ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        autoSkipPadding: 30,
                    }
                },
                y_temp: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                },
                y_vcc: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    }
                }
            }
        }
    )

    const statChart2 = new Chart(stat2, {
        data: {
            datasets: [{
                type: 'line',
                label: 'WiFi connection RSSI [ dB ]',
                data: rssi,
            },],
            labels: labels_stat
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            animations: false,
            datasets: {
                line: {
                    pointRadius: 0 // disable for all `'line'` datasets
                }
            },
            plugins: {
                decimation: {
                    enabled: true,
                }
            },
            interaction: {
                mode: 'nearest',
                intersect: false,
            },
            scales: {
                x: {
                    ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        autoSkipPadding: 30,
                    }
                },
            }
        }
    })
}

statGraphs()
