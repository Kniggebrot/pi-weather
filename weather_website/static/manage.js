// Select all (editable) permission checkboxes, add event listeners to each, so that on change the reset and send settings buttons can be displayed

function editButtonsSetup() {
    const allPermChecks = document.querySelectorAll(".userperms input[type=checkbox]:not([disabled])")

    allPermChecks.forEach((el) => {
        el.addEventListener('input', showEditButtons)
    })
}

function showEditButtons(event) {
    /** @type Element */
    const firedFrom = event.target
    const fromID = firedFrom.id

    const user = /(?<=^\w*-).*/.exec(fromID)[0]

    const button1 = document.getElementById("confirm-" + user)
    const button2 = document.getElementById("reset-" + user)

    button1.disabled = false
    button2.disabled = false

    button1.onclick = (event) => {
        const form = event.target.parentNode.parentNode.parentNode

        putData = new FormData(form)

        fetch(event.target.formAction, { "method": "PUT", "body": putData })
            .then((response) => {
                if (response.ok) {
                    window.location.assign(response.url)
                } else {
                    throw new Error(response.statusText)
                }
            })
            .catch((error) => {
                console.log("Error on setting permissions: " + error)
            })

        event.preventDefault()
    }

    button2.onclick = (event) => {
        const ownID = event.target.id
        const user = /(?<=^reset-).*/.exec(ownID)[0]

        // Since values were reset, disable both buttons again
        const button1 = document.getElementById("confirm-" + user)
        const button2 = event.target

        button1.disabled = true
        button2.disabled = true
    }
}

function deleteUser(caller) {
    const ownID = caller.id
    const user = /(?<=^delete-).*/.exec(ownID)[0]

    const confirmed = confirm("Are you sure you want to delete user " + user +"?")
    if (confirmed)
    {
        fetch("/user/" + encodeURIComponent(user) + "/", { "method": "DELETE" })
            .then((response) => {
                if (response.ok) {
                    window.location.assign(response.url)
                } else {
                    throw new Error(response.statusText)
                }
            })
            .catch((error) => {
                console.log("Error on setting permissions: " + error)
            })
    }
}

editButtonsSetup()
