function postLogout() {
    // Send a Post request to logout to reset your session and thus log out
    fetch("/user/logout", { "method": "POST", })
        .then((response) => {
            if (response.ok) {
                window.location.assign(response.url)
            } else {
                throw new Error(response.statusText)
            }
        })
        .catch((error) => {
            console.log("Error on logout: " + error)
        })
}

async function populateNavbar() {
    // Fetch devicelist, incorporate it into devices dropdown in navbar
    const devlist = await fetch("/api/devices/")
        .then( (response) => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error(response.statusText)
            }
        })
        .then( (parsed) => parsed.data)
        .catch( (error) => {
            console.log("Error fetching device list: "+ error)
            return []
        })

    if (devlist.length > 0 && window.location.pathname == "/") homeExtension(devlist)

    try {
        const develements = devlist.map( (el) => {
            const wrapper = document.createElement('li')
            const link = document.createElement('a')
            link.innerText = (el.name ? `${el.name} (${el.id})` : el.id)
            link.href = "/devices/" + el.id
            link.className = "dropdown-item"

            wrapper.append(link)
            return wrapper
        })

        const deviceDropdown = document.getElementById("devDropdownList")
        develements.forEach(element => {
            deviceDropdown.append(element)
        })
    } catch (error) {
        console.log("Error assigning devices to navbar: "+ error)
    }
}

async function homeExtension(devicelist) {
    // If on dashboard, add links to each device "card" into a dropdown
    const homeLink = document.getElementById("homeNavbar")
    const homeContainer = homeLink.parentElement

    // Prepare dropdown list
    const dashboardList = document.createElement("ul")
    dashboardList.className = "dropdown-menu"
    dashboardList.setAttribute("aria-labelledby", "homeNavbarDesc")

    const descContainer = document.createElement("li")
    const descText = document.createElement("span")
    descText.id = "homeNavbarDesc"
    descText.className = "dropdown-header"
    descText.innerText = "Jump to device:"
    descContainer.append(descText)
    dashboardList.append(descContainer)

    /*const descContainer2 = document.createElement("li")
    const descHr = document.createElement("hr")
    descHr.className = "dropdown-divider"
    descContainer2.append(descText)
    dashboardList.append(descContainer2)*/

    const dashDevList = devicelist.map( (el) => {
        const wrapper = document.createElement('li')
        const link = document.createElement('a')
        link.innerText = (el.name ? `${el.name} (${el.id})` : el.id)
        link.href = `#${el.id}`
        link.className = "dropdown-item"

        wrapper.append(link)
        return wrapper
    })

    dashDevList.forEach( el => dashboardList.append(el) )

    // Now, edit homeNavbar and its parent to display as a dropdown menu, and append dashboardList
    homeContainer.className += " dropdown"
    homeLink.className += " dropdown-toggle"
    homeLink.setAttribute("role", "button")
    homeLink.setAttribute("data-bs-toggle", "dropdown")
    homeLink.setAttribute("aria-expanded", "false")
    homeContainer.append(dashboardList)
    
}


function highlightActive() {
    // Highlight active page in navbar
    try {
        const activeLink = document.querySelector("nav a.nav-link[href='" + window.location.pathname + "']") // Select nav element linking to current page
        activeLink.className += " active"
        activeLink.ariaCurrent = "page"
    } catch (error) {
        try {
            // Maybe the parent is available in the navbar?
            const parent = window.location.pathname.split('/').slice(0, -1).join('/')
            const activeLink = document.querySelector("nav a.nav-link[href='" + parent + "']") // Select nav element linking to current page
            activeLink.className += " active"
            activeLink.ariaCurrent = "page"
        } catch (error) {
            console.debug("Error trying to set active navlink: " + error)
        }
    }
}

populateNavbar()
highlightActive()
