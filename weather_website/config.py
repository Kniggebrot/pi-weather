"""
An example instance config for your installation.
By default, the secret key for your production environment can be set here; other settings, such as DB locations, can be set here as well.
When done, move to your instance folder, either locally or in your docker container.

Don't forget to generate your own unique secret key! See https://flask.palletsprojects.com/en/2.1.x/tutorial/deploy/#configure-the-secret-key

Of course, python functions can be used here as well, e. g. os.path.join for joining paths for a DB.
"""
# Uncomment line, and replace 'dev' with your secret key (as a string)
#SECRET_KEY = 'dev'
#MQTTHOST='mqtt'
#PROXIED=True
DEBUG=False
TESTING=False
