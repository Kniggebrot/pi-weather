#!/bin/sh
# Setup Docker secrets
# Should only be run once; if certificates need to be replaced, better comment out the secrets that dont need to be replaced
# Requires docker in swarm mode

echo This script requires the following secrets already created via openssl or a certificate/key generator of your choosing:
echo    - mqtt/certs/ca.crt
echo    - mqtt/certs/server.crt
echo    - mqtt/certs/server.key
echo 
echo    "- nginx/certs/dhparam.pem   Can also be an (almost) empty file if you don't wish to use it for nginx"
echo    - nginx/certs/nginx.crt
echo    - nginx/certs/nginx.key

echo 
echo "Don't forget to (un)comment the secrets you'd like to replace! If generating, leave everything in the script as it is."
echo Ready? Press ENTER

read ok

docker swarm init
# MQTT secrets
echo 
echo Generating MQTT CA secret...
docker secret create mqcerts1 ./mqtt/certs/ca.crt 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm mqcerts1
    docker secret create mqcerts1 ./mqtt/certs/ca.crt
    echo Replaced!
else
    echo Generated!
fi

echo Generating MQTT server crt secret...
docker secret create mqcerts2 ./mqtt/certs/server.crt 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm mqcerts2
    docker secret create mqcerts2 ./mqtt/certs/server.crt
    echo Replaced!
else
    echo Generated!
fi

echo Generating MQTT server key secret...
docker secret create mqcerts3 ./mqtt/certs/server.key 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm mqcerts3
    docker secret create mqcerts3 ./mqtt/certs/server.key
    echo Replaced!
else
    echo Generated!
fi

# NGINX secrets
echo 
echo Generating nginx crt secret...
docker secret create nginxcerts1 ./nginx/certs/nginx.crt 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm nginxcerts1
    docker secret create nginxcerts1 ./nginx/certs/nginx.crt
    echo Replaced!
else
    echo Generated!
fi

echo Generating nginx key secret...
docker secret create nginxcerts2 ./nginx/certs/nginx.key 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm nginxcerts2
    docker secret create nginxcerts2 ./nginx/certs/nginx.key
    echo Replaced!
else
    echo Generated!
fi

echo Generating nginx diffie-hellman secret...
docker secret create nginxcerts3 ./nginx/certs/dhparam.pem 2>/dev/null
if [ $? -ne 0 ]
then
    docker secret rm nginxcerts3
    docker secret create nginxcerts3 ./nginx/certs/dhparam.pem
    echo Replaced!
else
    echo Generated!
fi
echo Done!
