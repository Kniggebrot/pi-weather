import setuptools

setuptools.setup(
    name='weather_website',
    author="Florian Knigge",
    description="Receive esp-sensor measurements via MQTT and display them on a website via Flask.",
    version='0.1.0',
    packages=setuptools.find_namespace_packages(
        where=".",
        include=["weather_website*",],
        exclude=["weather_website.tests",],
    ),
    package_dir={"": "."},
    package_data={
        "weather_website.static": ["*.css", "*.js", "*.map"],
        "weather_website.static.bootstrap.css": ["*.css", "*.map"],
        "weather_website.static.bootstrap.js": ["*.js", "*.map"],
        "weather_website.static.chartjs.dist": ["*.js"],
        "weather_website.static.devices": ["*.css", "*.js","*.map"],
        "weather_website.static.icons": ["*.svg"],

        "weather_website.templates": ["*.html"],
        "weather_website.templates.auth": ["*.html"],
    },
    exclude_package_data={"": ["*.pyc", "*.pyo", "*.pyd"]},
    zip_safe=False,
    install_requires=[
        'flask',
        'paho-mqtt',
        'waitress',
    ],
    extras_require={
        'uWSGI': ['pyuwsgi',],
    },
    entry_points={
        'console_scripts': [
            "launch-backend=weather_website.backend:launch_from_cli",
        ],
    },
)
